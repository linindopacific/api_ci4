# LPI:Auth

## Requirements

- PHP 7.3+, 8.0+
- myth/auth 1.0+
- lcobucci/jwt 3

## Installation
Please add this to \Config\Auth at authenticationLibs
    'jwt' => 'LPI\Auth\Authentication\JwtAuthenticator'