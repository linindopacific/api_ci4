<?php

use LPI\Auth\Authorization\RemoteAuthorization;
use LPI\Auth\Entities\User;
use LPI\Auth\Models\LoginModel;
use LPI\Auth\Models\UserModel;

if (! function_exists("logged_in"))
{
	/**
	 * Checks to see if the user is logged in.
	 *
	 * @return bool
	 */
	function logged_in()
	{
		return service("authentication", "local", new UserModel(), new LoginModel())->check();
	}
}

if (! function_exists("user"))
{
	/**
	 * Returns the User instance for the current logged in user.
	 *
	 * @return User|null
	 */
	function user()
	{
		$authenticate = service("authentication", "local", new UserModel(), new LoginModel());
		$authenticate->check();
		return $authenticate->user();
	}
}

if (! function_exists("user_id"))
{
	/**
	 * Returns the User ID for the current logged in user.
	 *
	 * @return int|null
	 */
	function user_id()
	{
		$authenticate = service("authentication", "local", new UserModel(), new LoginModel());
		$authenticate->check();
		return $authenticate->id();
	}
}

if (! function_exists("in_groups"))
{
	/**
	 * Ensures that the current user is in at least one of the passed in
	 * groups. The groups can be passed in as either ID"s or group names.
	 * You can pass either a single item or an array of items.
	 *
	 * Example:
	 *  in_groups([1, 2, 3]);
	 *  in_groups(14);
	 *  in_groups("admins");
	 *  in_groups( ["admins", "moderators"] );
	 *
	 * @param mixed  $groups
	 *
	 * @return bool
	 */
	function in_groups($groups): bool
	{
		$authenticate = service("authentication", "local", new UserModel(), new LoginModel());
        $authorize    = service("authorization", null, null, new UserModel());

        if ($authenticate->check())
        {
            return $authorize->inGroup($groups, $authenticate->id());
        }

        return false;
	}
}

if (! function_exists("has_permission"))
{
	/**
	 * Ensures that the current user has the passed in permission.
	 * The permission can be passed in either as an ID or name.
	 *
	 * @param int|string $permission
	 *
	 * @return bool
	 */
	function has_permission($permission): bool
	{
		$authenticate = service("authentication", "local", new UserModel(), new LoginModel());
        $authorize    = service("authorization", null, null, new UserModel());

        if ($authenticate->check())
        {
            return $authorize->hasPermission($permission, $authenticate->id()) ?? false;
        }

        return false;
	}
}

if (! function_exists("logged_in_jwt"))
{
	/**
	 * Checks to see if the user is logged in.
	 *
	 * @return bool
	 */
	function logged_in_jwt()
	{
		return service("authentication", "jwt")->check();
	}
}

if (! function_exists("user_jwt"))
{
	/**
	 * Returns the User instance for the current logged in user.
	 *
	 * @return User|null
	 */
	function user_jwt()
	{
		$authenticate = service("authentication", "jwt");
		$authenticate->check();
		return $authenticate->user();
	}
}

if (! function_exists("user_id_jwt"))
{
	/**
	 * Returns the User ID for the current logged in user.
	 *
	 * @return int|null
	 */
	function user_id_jwt()
	{
		$authenticate = service("authentication", "jwt");
		$authenticate->check();
		return $authenticate->id();
	}
}

if (! function_exists("in_groups_jwt"))
{
	/**
	 * Ensures that the current user is in at least one of the passed in
	 * groups. The groups can be passed in as either ID"s or group names.
	 * You can pass either a single item or an array of items.
	 *
	 * Example:
	 *  in_groups([1, 2, 3]);
	 *  in_groups(14);
	 *  in_groups("admins");
	 *  in_groups( ["admins", "moderators"] );
	 *
	 * @param mixed  $groups
	 *
	 * @return bool
	 */
	function in_groups_jwt($groups): bool
	{
		$authenticate = service("authentication", "jwt");
        if ($authenticate->check())
        {
			$authorize = new RemoteAuthorization();
			return $authorize->inGroup($groups, $authenticate->id());
        }

        return false;
	}
}

if (! function_exists("has_permission_jwt"))
{
	/**
	 * Ensures that the current user has the passed in permission.
	 * The permission can be passed in either as an ID or name.
	 *
	 * @param int|string $permission
	 *
	 * @return bool
	 */
	function has_permission_jwt($permission): bool
	{
		$authenticate = service("authentication", "jwt");
        $authorize = service("authorization", null, null, new UserModel());

        if ($authenticate->check())
        {
			$authorize = new RemoteAuthorization();
			return $authorize->hasPermission($permission, $authenticate->id());
        }

        return false;
	}
}