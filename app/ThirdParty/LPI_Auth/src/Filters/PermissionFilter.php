<?php

namespace LPI\Auth\Filters;

/*
 * File: PermissionFilter.php
 * Project: LPI:Auth
 * File Created: Friday, 24th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 24th September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

class PermissionFilter extends \Myth\Auth\Filters\PermissionFilter
{
	//--------------------------------------------------------------------

}