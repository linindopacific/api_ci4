<?php

namespace LPI\Auth\Filters;

/*
 * File: JwtLoginFilter.php
 * Project: LPI:Auth
 * File Created: Tuesday, 7th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 11th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Config\App;

class JwtLoginFilter implements FilterInterface
{
	/**
	 * Verifies that a user is logged in, or redirects to login.
	 *
	 * @param RequestInterface $request
	 * @param array|null $params
	 *
	 * @return mixed
	 */
	public function before(RequestInterface $request, $params = null)
	{
		if (! function_exists('logged_in_jwt'))
		{
			helper('auth');
		}

		$current = (string)current_url(true)
			->setHost('')
			->setScheme('')
			->stripQuery('token');

		$config = config(App::class);
		if($config->forceGlobalSecureRequests)
		{
			# Remove "https:/"
			$current = substr($current, 7);
		}

		// Make sure this isn't already a login route
		if (in_array((string)$current, [route_to('login'), route_to('forgot'), route_to('reset-password'), route_to('register'), route_to('activate-account')]))
		{
			return;
		}

		// if no user is logged in then send to the login form
		if (! logged_in_jwt())
		{
			session()->set('redirect_url', current_url());
			return redirect('login');
		}
	}

	/**
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param array|null $arguments
	 *
	 * @return void
	 */
	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
	{
	}
}