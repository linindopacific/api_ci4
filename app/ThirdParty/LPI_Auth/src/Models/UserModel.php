<?php

namespace LPI\Auth\Models;

/*
 * File: UserModel.php
 * Project: LPI:Auth
 * File Created: Monday, 6th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Auth\Entities\User;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Myth\Auth\Models\UserModel as MythModel;

class UserModel extends MythModel
{
    protected $allowedFields = [
        'email', 'username', 'password_hash', 'reset_hash', 'reset_at', 'reset_expires', 'activate_hash',
        'status', 'status_message', 'active', 'force_pass_reset', 'deleted_at',
        'uid', 'firstname', 'lastname', 'phone', 'tmk_at', 'last_active_at'
    ];

    protected $returnType = User::class;

	/**
     * Get an unused ID for user creation
     *
     * @return  int between 1200 and 4294967295
     */
    public function get_unused_id()
    {
        // Create a random user id between 1200 and 4294967295
        $random_unique_int = 2147483648 + mt_rand( -2147482448, 2147483647 );

        // Make sure the random uid isn't already in use
        $query = $this->where('uid', $random_unique_int )->first();

        if ( ! empty($query) )
        {
            // If the random uid is already in use, try again
            return $this->get_unused_id();
        }

        return $random_unique_int;
    }

    /**
     * Generate a random password
     *
     * @return string
     */
    public function get_random_password()
    {
        $config = new \Config\Auth();

        $generator = new ComputerPasswordGenerator();
        $generator
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength($config->minimumPasswordLength);

        return $generator->generatePasswords()[0];
    }
}