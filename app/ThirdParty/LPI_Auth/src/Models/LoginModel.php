<?php

namespace LPI\Auth\Models;

/*
 * File: LoginModel.php
 * Project: LPI:Auth
 * File Created: Friday, 24th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 24th September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use Myth\Auth\Models\LoginModel as MythLogin;

class LoginModel extends MythLogin
{
    protected $allowedFields = [
        'ip_address', 'email', 'user_id', 'user_agent', 'date', 'success'
    ];


    protected $validationRules = [
        'ip_address' => 'required',
        'email'      => 'required',
        'user_id'    => 'permit_empty|integer',
        'user_agent' => 'permit_empty|string',
        'date'       => 'required|valid_date',
    ];
}