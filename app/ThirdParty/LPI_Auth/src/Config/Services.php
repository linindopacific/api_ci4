<?php

namespace LPI\Auth\Config;

/*
 * File: Services.php
 * Project: LPI:Auth
 * File Created: Monday, 6th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 12th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Config\BaseService;
use CodeIgniter\Model;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key\InMemory;
use LPI\Auth\Config\Auth as AuthConfig;
use LPI\Auth\Models\LoginModel;
use LPI\Auth\Models\UserModel;
use Myth\Auth\Authorization\FlatAuthorization;
use Myth\Auth\Authorization\GroupModel;
use Myth\Auth\Authorization\PermissionModel;

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends BaseService
{
	public static function authentication(string $lib = 'local', Model $userModel = null, Model $loginModel = null, bool $getShared = true)
	{
		if ($getShared)
		{
			return self::getSharedInstance('authentication', $lib, $userModel, $loginModel);
		}

		$userModel  = $userModel ?? model(UserModel::class);
		$loginModel = $loginModel ?? model(LoginModel::class);

		/** @var AuthConfig $config */
		$config   = config('Auth');
		$class	  = $config->authenticationLibs[$lib];
		$instance = new $class($config);

		return $instance
			->setUserModel($userModel)
			->setLoginModel($loginModel);
	}

	public static function authorization(Model $groupModel = null, Model $permissionModel = null, Model $userModel = null, bool $getShared = true)
	{
		if ($getShared)
		{
			return self::getSharedInstance('authorization', $groupModel, $permissionModel, $userModel);
		}

		$groupModel	     = $groupModel ?? model(GroupModel::class);
		$permissionModel = $permissionModel ?? model(PermissionModel::class);
		$userModel	     = $userModel ?? model(UserModel::class);

		$instance = new FlatAuthorization($groupModel, $permissionModel);

		return $instance->setUserModel($userModel);
	}

	/**
	 * Lcobucci configuration
	 *
	 * @param array $config
	 * @param boolean $getShared
	 * @return Configuration
	 */
	public static function lcobucci(array $config = null, bool $getShared = true)
	{
		if ($getShared)
		{
			return static::getSharedInstance('lcobucci', $config);
		}

		if ($config)
		{
			if ($config['algorithm'] === 'asymmetric')
			{
				if ($config['method'] === 'sign')
				{
					$configuration =  Configuration::forAsymmetricSigner(
						new Signer\Rsa\Sha256(),
						InMemory::file(ROOTPATH . 'rsa_private.pem'),
						InMemory::base64Encoded(env('JWT_KEY'))
					);
				}
				elseif ($config['method'] === 'verify')
				{
					$configuration =  Configuration::forAsymmetricSigner(
						new Signer\Rsa\Sha256(),
						InMemory::file(FCPATH . 'rsa_public.pem'),
						InMemory::base64Encoded(env('JWT_KEY'))
					);
				}
			}
			elseif ($config['algoritm'] === 'symmetric')
			{
				$configuration = Configuration::forSymmetricSigner(
					new Signer\Rsa\Sha256(),
					InMemory::base64Encoded(env('JWT_KEY'))
				);
			}
		}
		else
		{
			$configuration = Configuration::forUnsecuredSigner();
		}

		return $configuration;
	}

}