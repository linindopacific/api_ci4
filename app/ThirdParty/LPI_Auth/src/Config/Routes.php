<?php

/*
 * LPI:Auth routes file.
 */
$routes->group('', ['namespace' => 'LPI\Auth\Controllers'], function ($routes) {
    // Login/out
    $routes->get("login", "AuthController::login", ["as" => "login"]);
    $routes->post("login", "AuthController::attemptLogin");
    $routes->get("logout", "AuthController::logout");

    // Forgot/Resets
    $routes->get("forgot", "AuthController::forgotPassword", ["as" => "forgot"]);
    $routes->post("forgot", "AuthController::attemptForgot");
    $routes->get("reset-password", "AuthController::resetPassword", ["as" => "reset-password"]);
    $routes->post("reset-password", "AuthController::attemptReset");
});
