<?php

namespace LPI\Auth\Config;

/*
 * File: Auth.php
 * Project: LPI:Auth
 * File Created: Monday, 6th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use Myth\Auth\Config\Auth as AuthConfig;

class Auth extends AuthConfig
{
	public $theme = 'metroui';

	/**
	 * --------------------------------------------------------------------
	 * Default User Group
	 * --------------------------------------------------------------------
	 *
	 * The name of a group a user will be added to when they register,
	 * i.e. $defaultUserGroup = 'guests'.
	 *
	 * @var string
	 */
	public $defaultUserGroup = 'guests';

	/**
	 * --------------------------------------------------------------------
	 * Libraries
	 * --------------------------------------------------------------------
	 *
	 * @var array
	 */
	public $authenticationLibs = [
		'local' => 'LPI\Auth\Authentication\LocalAuthenticator',
		'jwt' => 'LPI\Auth\Authentication\JwtAuthenticator',
	];

	/**
	 * --------------------------------------------------------------------
	 * Allow Persistent Login Cookies (Remember me)
	 * --------------------------------------------------------------------
	 *
	 * While every attempt has been made to create a very strong protection
	 * with the remember me system, there are some cases (like when you
	 * need extreme protection, like dealing with users financials) that
	 * you might not want the extra risk associated with this cookie-based
	 * solution.
	 *
	 * @var bool
	 */
	public $allowRemembering = false;

	/**
	 * --------------------------------------------------------------------
	 * Require Confirmation Registration via Email
	 * --------------------------------------------------------------------
	 *
	 * When enabled, every registered user will receive an email message
	 * with an activation link to confirm the account.
	 *
	 * @var string|null Name of the ActivatorInterface class
	 */
	public $requireActivation = null;
}