<?php

namespace LPI\Auth\Config;

/*
 * File: Events.php
 * Project: LPI:Auth
 * File Created: Friday, 10th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 10th September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Events\Events;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\I18n\Time;

Events::on('pre_system', function () {

	Events::on('ApiQuery', function(RequestInterface $request, int $status, int $user_id = NULL)
	{
		$contentType = $request->getHeaderLine('content-type');
		$body = null;
		if ($contentType === 'application/json')
		{
			$_body = $request->getJSON(true);
			if (in_array($request->uri->getPath(), ['v1/login', 'v1/user']))
			{
				unset($_body['password']);
			}

			$body = json_encode($_body);
		}
		else
		{
			$body = json_encode($request->getPost());
		}

		$myTime = new Time('now');

		$path = $request->uri->getPath();
		$query = $request->uri->getQuery();
		$fullPath = ($query) ? $path . '?' . $query : $path;

		$data = [
			'method' => $request->getMethod(),
			'path' => $path,
			'body' => $body,
			'user_id' => $user_id,
			'ip_address' => $request->getIPAddress(),
			'status' => $status,
			'request_at' => $myTime->toDateTimeString()
		];
		$model = new \LPI\Auth\Models\RequestLogEntryModel();
		if(! $model->save($data) ) {
			log_message('error', implode(' ', $model->errors));
		}
	});
});
