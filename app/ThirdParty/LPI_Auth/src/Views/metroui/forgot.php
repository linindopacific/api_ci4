<?= $this->extend("App\ThirdParty\LPI_Auth\src\Views\metroui\layout") ?>

<?= $this->section("main") ?>

    <form action="<?= route_to('forgot') ?>" method="post"
            class="forgot-form bg-white p-6 mx-auto border bd-default win-shadow"
            data-role="validator"
            data-clear-invalid="3000"
            data-interactive-check="true"
            data-on-validate-form="validateForm">
        <?= csrf_field() ?>
        <span class="mif-vpn-lock mif-4x place-right" style="margin-top: -10px;"></span>
        <h2 class="text-light"><?=lang("Auth.forgotPassword")?></h2>
        <hr class="thin mt-4 mb-4 bg-white">
        <div class="form-group">
            <input type="text" data-role="input" data-prepend="<span class='mif-user'>" placeholder="<?=lang("Auth.username")?>" data-validate="required number" name="username" autofocus>
            <span class="invalid_feedback">
                <?= lang("Validation.numeric", ["field" => lang("Auth.username")]) ?>
            </span>
        </div>
        <div class="form-group mt-10">
            <a class="button link flat-button place-right" href="<?= route_to('login') ?>" role="button"><?=lang("Auth.loginTitle")?></a>
            <button type="submit" class="button"><?=lang("Auth.sendInstructions")?></button>
        </div>
    </form>

<?= $this->endSection() ?>