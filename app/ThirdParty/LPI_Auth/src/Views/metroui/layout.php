<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="MIS7102">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" href="<?= base_url("favicon.ico"); ?>">
    <title>LPI:Auth</title>
    <link href="https://cdn.metroui.org.ua/v4/css/metro-all.min.css" rel="stylesheet">
    <?= $this->renderSection("pageStyles") ?>
    <style>
        form {
            width: 350px;
            height: auto;
            top: 50%;
            margin-top: -160px;
        }
    </style>
    <script src="https://cdn.metroui.org.ua/v4/js/metro.min.js"></script>
</head>
<body class="h-vh-100 bg-brandColor2">

    <?= $this->renderSection("main") ?>

    <?= $this->renderSection("pageScripts") ?>
    <script type="text/javascript">
        function validateForm(){
            $("form").animate({
                opacity: 0
            });
        }

        <?php if (session()->has('errors')) : ?>
            var errors = "";
            <?php foreach (session('errors') as $error) : ?>
                errors += "<?= $error ?>";
            <?php endforeach ?>
            Metro.toast.create(errors, null, null, "alert", {timeout: 5000});
        <?php endif ?>
        <?php if (session()->has("error")) : ?>
            Metro.toast.create("<?= session('error') ?>", null, null, "alert", {timeout: 5000});
        <?php endif ?>
        <?php if (session()->has("message")) : ?>
            Metro.toast.create("<?= session('message') ?>", null, null, "success", {timeout: 5000});
        <?php endif ?>
    </script>
</body>
</html>