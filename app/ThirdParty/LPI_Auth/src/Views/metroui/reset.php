<?= $this->extend("App\ThirdParty\LPI_Auth\src\Views\metroui\layout") ?>

<?= $this->section("main") ?>

    <form action="<?= route_to('reset-password') ?>" method="post"
            class="reset-form bg-white p-6 mx-auto border bd-default win-shadow"
            data-role="validator"
            data-clear-invalid="3000"
            data-interactive-check="true"
            data-on-validate-form="validateForm">
        <?= csrf_field() ?>
        <span class="mif-vpn-lock mif-4x place-right" style="margin-top: -10px;"></span>
        <h2 class="text-light"><?=lang("Auth.resetYourPassword")?></h2>
        <hr class="thin mt-4 mb-4 bg-white">
        <div class="form-group">
            <input type="text" data-role="input" data-prepend="<span class='mif-shrink2'>" placeholder="<?=lang("Auth.token")?>" data-validate="required" name="token" value="<?= old('token', $token ?? '') ?>" autofocus>
            <span class="invalid_feedback">
                <?= lang("Validation.required", ["field" => lang("Auth.token")]) ?>
            </span>
        </div>
        <div class="form-group">
            <input type="text" data-role="input" data-prepend="<span class='mif-user'>" placeholder="<?=lang("Auth.username")?>" data-validate="required number" name="username" value="<?= old('username') ?>">
            <span class="invalid_feedback">
                <?= lang("Validation.numeric", ["field" => lang("Auth.username")]) ?>
            </span>
        </div>
        <div class="form-group">
            <input type="password" data-role="input" data-prepend="<span class='mif-key'>" placeholder="<?=lang('Auth.password')?>" data-validate="required" name="password">
            <span class="invalid_feedback">
                <?= lang("Validation.required", ["field" => lang('Auth.password')]) ?>
            </span>
        </div>
        <div class="form-group">
            <input type="password" data-role="input" data-prepend="<span class='mif-key'>" placeholder="<?=lang('Auth.newPasswordRepeat')?>" data-validate="required compare=password" name="pass_confirm">
        </div>
        <div class="form-group mt-10">
            <button type="submit" class="button"><?=lang("Auth.resetPassword")?></button>
        </div>
    </form>

<?= $this->endSection() ?>