<?= $this->extend("App\ThirdParty\LPI_Auth\src\Views\metroui\layout") ?>

<?= $this->section("main") ?>
    <form action="<?= route_to('login') ?>" method="post"
          class="login-form bg-white p-6 mx-auto border bd-default win-shadow"
          data-role="validator"
          data-clear-invalid="3000"
          data-interactive-check="true"
          data-on-validate-form="validateForm">
        <span class="mif-vpn-lock mif-4x place-right" style="margin-top: -10px;"></span>
        <h2 class="text-light"><?=lang('Auth.loginTitle')?></h2>
        <hr class="thin mt-4 mb-4 bg-white">
        <div class="form-group">
            <input type="text" data-role="input" data-prepend="<span class='mif-user'>" placeholder="<?=lang('Auth.emailOrUsername')?>" data-validate="required" name="login" autofocus>
            <span class="invalid_feedback">
                <?= lang("Validation.required", ["field" => lang('Auth.emailOrUsername')]) ?>
            </span>
        </div>
        <div class="form-group">
            <input type="password" data-role="input" data-prepend="<span class='mif-key'>" placeholder="<?=lang('Auth.password')?>" data-validate="required minlength=8" name="password">
            <span class="invalid_feedback">
                <?= lang("Validation.required", ["field" => lang('Auth.password')]) ?>
            </span>
        </div>
        <div class="form-group mt-10">
            <a class="button link flat-button place-right" href="<?= route_to('forgot') ?>" role="button"><?=lang('Auth.forgotYourPassword')?></a>
            <button type="submit" class="button"><?=lang('Auth.loginAction')?></button>
        </div>
    </form>

<?= $this->endSection() ?>