<?php

namespace LPI\Auth\Authentication;

/*
 * File: JwtAuthenticator.php
 * Project: LPI:Auth
 * File Created: Monday, 6th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Events\Events;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\LooseValidAt;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\RequiredConstraintsViolated;
use LPI\Auth\Config\Auth as AuthConfig;
use LPI\Auth\Entities\User;
use Myth\Auth\Authentication\AuthenticationBase;
use Myth\Auth\Authentication\AuthenticatorInterface;
use Myth\Auth\Entities\User as MythUser;

class JwtAuthenticator extends AuthenticationBase implements AuthenticatorInterface
{
    protected $base_uri = "";

    /**
     * @var AuthConfig
     */
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        $this->base_uri = env("API_URI") ?? "https://api.linindo.com/v1/";
    }

    /**
     * Attempts to validate the credentials and log a user in.
     *
     * @param array $credentials
     * @param bool  $remember Should we remember the user (if enabled)
     *
     * @return bool
     */
    public function attempt(array $credentials, bool $remember = null): bool
    {
        $this->user = $this->validate($credentials, true);

        if (empty($this->user))
        {
            $this->user = null;
            return false;
        }

        if ($this->user->isBanned())
        {
            $this->error = lang("Auth.userIsBanned");

            $this->user = null;
            return false;
        }

        if (! $this->user->isActivated())
        {
            $param = http_build_query([
                "login" => urlencode($credentials["login"])
            ]);

            $this->error = lang("Auth.notActivated") ." ". anchor(route_to("resend-activate-account")."?".$param, lang("Auth.activationResend"));

            $this->user = null;
            return false;
        }

        return $this->login($this->user, true);
    }

    /**
     * Checks to see if the user is logged in or not.
     *
     * @return bool
     */
    public function check(): bool
    {
        if ($this->isLoggedIn())
        {
            return true;
        }

        return false;
    }

    /**
     * Checks the user"s credentials to see if they could authenticate.
     * Unlike `attempt()`, will not log the user into the system.
     *
     * @param array $credentials
     * @param bool  $returnUser
     *
     * @return bool|User
     */
    public function validate(array $credentials, bool $returnUser=false)
    {
        if (! empty($credentials["token"]) && ! empty($credentials["login"]))
        {
            $this->error = "Please use one credential; token or login";
            return false;
        }

        if (! empty($credentials["token"])) // token
        {
            $user = $this->getTokenInfo($credentials["token"], $returnUser);

            if (! $user)
            {
                return false;
            }

            return $returnUser
                ? $user
                : true;
        }
        elseif (! empty($credentials["login"])) // login & password
        {
            $request = \Config\Services::request();
            $agent = $request->getUserAgent();
            if ($agent->isBrowser()) {
                $currentAgent = $agent->getBrowser() . ' ' . $agent->getVersion() . ' on ' . $agent->getPlatform();
            } elseif ($agent->isRobot()) {
                $currentAgent = $agent->getRobot();
            } elseif ($agent->isMobile()) {
                $currentAgent = $agent->getMobile();
            } else {
                $currentAgent = $agent->getAgentString();
            }

            try
            {
                $client = \Config\Services::curlrequest(["base_uri" => $this->base_uri]);
                $response = $client->post("login", [
                    "json" => [
                        "login" => $credentials["login"] ?? "",
                        "password" => $credentials["password"] ?? "",
                    ],
                    "http_errors" => false,
                    "verify" => false,
                    "user_agent" => $currentAgent
                ]);

                $body = $response->getBody();
                $code = (int)$response->getStatusCode();
                if (strpos($response->getHeader("content-type"), "application/json") !== false)
                {
                    $body = json_decode($body);
                }

                if ($code === 200)
                {
                    $id = $body->uid;
                    $access_token = $body->access_token;
                    $expires = $body->expires;

                    $session = service("session");
                    $session->setTempdata("access_token", $access_token, $expires);

                    // Can we find a user with those credentials?
                    $user = $this->getUserInfo($id, $access_token);

                    if (! $user)
                    {
                        return false;
                    }

                    return $returnUser
                        ? $user
                        : true;
                }
                else
                {
                    $this->error = $body->messages->error;
                    return false;
                }
            }
            catch (\Exception $e)
            {
                $this->error = $e->getMessage();
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    // Check if valid token
    private function getTokenInfo(string $token, bool $returnUser = false)
    {
        $config = service("lcobucci", ["algorithm" => "asymmetric", "method" => "verify"]);

        try {
            $tokenParsed = $config->parser()->parse($token);
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

		$clock = new SystemClock(new \DateTimeZone(env("app.appTimezone")));
		$config->setValidationConstraints(
			new LooseValidAt($clock, new \DateInterval("PT10S")),
			new SignedWith($config->signer(), $config->signingKey()),
			new IssuedBy("https://linindopacific.com")
		);

		$constraints = $config->validationConstraints();
        try {
            $config->validator()->assert($tokenParsed, ...$constraints);
        } catch (RequiredConstraintsViolated $e) {
            // list of constraints violation exceptions:
            foreach($e->violations() as $violation) {
                $this->error = $violation->getMessage();
            }
            return false;
        }

        $id = $tokenParsed->claims()->get("sub");

        return $returnUser
                ? $this->getUserInfo($id, $token)
                : true;
    }

    private function getUserInfo(int $id, string $token)
    {
        try
        {
            $client = \Config\Services::curlrequest(["base_uri" => $this->base_uri]);
            $response = $client->get("user/$id", [
                "headers" => [
                    "Authorization" => "Bearer " . $token,
                    "Content-Type" => "application/json"
                ],
                "json" => [],
                "http_errors" => false,
                "verify" => false
            ]);

            $body = $response->getBody();
            $code = (int)$response->getStatusCode();
            if (strpos($response->getHeader("content-type"), "application/json") !== false)
            {
                $body = json_decode($body);
            }

            if ($code === 200)
            {
                $body = $body[0];

                $user = new User();
                $user->id = $body->id;
                $user->uid = $body->uid;
                $user->email = $body->email;
                $user->firstname = $body->firstname;
                $user->lastname = $body->lastname;
                $user->status = $body->status;
                $user->status_message = $body->status_message;
                $user->active = $body->active;
                $user->force_pass_reset = $body->force_pass_reset;
                $user->nik = $body->nik;

                return $user;
            }

            return false;
        }
        catch (\Exception $e)
        {
            $this->error = $e->getMessage();
            return false;
        }
    }

    /**
     * Checks to see if the user is logged in.
     *
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        // On the off chance
        if ($this->user instanceof User)
        {
            return true;
        }

        $session = service("session");
        if ($access_token = $session->getTempdata("access_token"))
        {
            // Store our current user object
            $this->user = $this->getTokenInfo($access_token, true);

            return $this->user instanceof User;
        }

        return false;
    }

    /**
     * Logs a user into the system.
     * NOTE: does not perform validation. All validation should
     * be done prior to using the login method.
     *
     * @param MythUser $user
     * @param bool                     $remember
     *
     * @return bool
     * @throws \Exception
     */
    public function login(MythUser $user=null, bool $remember = false): bool
    {
        if (empty($user))
        {
            $this->user = null;
            return false;
        }

        $this->user = $user;

        // Regenerate the session ID to help protect against session fixation
        if (ENVIRONMENT !== 'testing')
        {
            session()->regenerate();
        }

        // Let the session know we're logged in
        session()->set('logged_in_jwt', $this->user->id);

        // When logged in, ensure cache control headers are in place
        service('response')->noCache();

        if ($remember && $this->config->allowRemembering)
        {
            $this->rememberUser($this->user->id);
        }

        // We'll give a 20% chance to need to do a purge since we
        // don't need to purge THAT often, it's just a maintenance issue.
        // to keep the table from getting out of control.
        if (mt_rand(1, 100) < 20)
        {
            $this->loginModel->purgeOldRememberTokens();
        }

		// trigger login event, in case anyone cares
		Events::trigger('login', $user);

        return true;
    }

    /**
     * Logs a user out of the system.
     */
    public function logout()
    {
        helper("cookie");

        // Destroy the session data - but ensure a session is still
        // available for flash messages, etc.
        if (isset($_SESSION))
        {
            foreach ($_SESSION as $key => $value)
            {
                $_SESSION[$key] = NULL;
                unset($_SESSION[$key]);
            }
        }

        // Regenerate the session ID for a touch of added safety.
        session()->regenerate(true);

        // Handle user-specific tasks
        if ($user = $this->user())
        {
            // Trigger logout event
            Events::trigger("logout", $user);

            $this->user = null;
        }
    }
}