<?php

namespace LPI\Auth\Authentication;

/*
 * File: LocalAuthenticator.php
 * Project: LPI:Auth
 * File Created: Wednesday, 22nd September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Auth\Entities\User;
use Myth\Auth\Authentication\LocalAuthenticator as MythAuthLocal;
use Myth\Auth\Exceptions\AuthException;
use Myth\Auth\Password;

class LocalAuthenticator extends MythAuthLocal
{
    /**
     * Checks the user's credentials to see if they could authenticate.
     * Unlike `attempt()`, will not log the user into the system.
     *
     * @param array $credentials
     * @param bool  $returnUser
     *
     * @return bool|User
     */
    public function validate(array $credentials, bool $returnUser=false)
    {
        // Can't validate without a password.
        if (empty($credentials['password']) || count($credentials) < 2)
        {
            return false;
        }

        // Only allowed 1 additional credential other than password
        $password = $credentials['password'];
        unset($credentials['password']);

        if (count($credentials) > 1)
        {
            throw AuthException::forTooManyCredentials();
        }

        // Ensure that the fields are allowed validation fields
        if (! in_array(key($credentials), $this->config->validFields))
        {
            throw AuthException::forInvalidFields(key($credentials));
        }

        // Can we find a user with those credentials?
        $user = $this->userModel->where($credentials)
                                ->first();

        if (! $user)
        {
            $this->error = lang('Auth.badAttempt');
            return false;
        }

        // Now, try matching the passwords.
        $check = $this->loginModel->where('success', 1)->where('user_id', $user->id)->first();
        // if 1st login? Check with standar password_verify like old aerps
        $verify = (! empty($check)) ? Password::verify($password, $user->password_hash) : password_verify($password, $user->password_hash);
        if (! $verify)
        {
            $this->error = lang('Auth.invalidPassword');
            return false;
        }

        // Check to see if the password needs to be rehashed.
        // This would be due to the hash algorithm or hash
        // cost changing since the last time that a user
        // logged in.
        if (Password::needsRehash($user->password_hash, $this->config->hashAlgorithm))
        {
            $user->password = $password;
            $this->userModel->save($user);
        }

        // Save current activity
        $user->last_active_at = date("Y-m-d H:i:s");
        $this->userModel->save($user);

        return $returnUser
            ? $user
            : true;
    }

    /**
     * Record a login attempt
     *
     * @param string      $email
     * @param string|null $ipAddress
     * @param int|null    $userID
     *
     * @param bool        $success
     *
     * @return bool|int|string
     */
    public function recordLoginAttempt(string $email, string $ipAddress=null, int $userID=null, bool $success)
    {
        $agent = service('request')->getUserAgent();
		if ($agent->isBrowser()) {
			$currentAgent = $agent->getBrowser() . ' ' . $agent->getVersion() . ' on ' . $agent->getPlatform();
		} elseif ($agent->isRobot()) {
			$currentAgent = $agent->getRobot();
		} elseif ($agent->isMobile()) {
			$currentAgent = $agent->getMobile();
		} else {
			$currentAgent = 'Unidentified User Agent';
		}

        return $this->loginModel->insert([
            'ip_address' => $ipAddress,
            'email' => $email,
            'user_id' => $userID,
            'user_agent' => $currentAgent,
            'date' => date('Y-m-d H:i:s'),
            'success' => (int)$success
        ]);
    }

}
