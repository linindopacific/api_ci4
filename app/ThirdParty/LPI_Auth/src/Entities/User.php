<?php

namespace LPI\Auth\Entities;

/*
 * File: User.php
 * Project: LPI:Auth
 * File Created: Monday, 6th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use Myth\Auth\Entities\User as MythEntity;

class User extends MythEntity
{
    /**
     * Default attributes.
     * @var array
     */
    protected $attributes = [
    	'firstname' => 'Guest',
    	'lastname'  => 'User',
    ];

	protected $casts = [
        'active' => 'boolean',
        'force_pass_reset' => 'boolean',
		'uid' => 'integer'
    ];

	protected $datamap = [
        'nik' => 'username',
    ];

	protected $dates = [
		'reset_at',
		'reset_expires',
		'last_active_at',
		'created_at',
		'updated_at',
		'deleted_at'
	];

	/**
	 * Returns a full name: "first last"
	 *
	 * @return string
	 */
	public function getName()
	{
		return trim(trim($this->attributes['firstname']) . ' ' . trim($this->attributes['lastname']));
	}

	public function setPhone(string $phone)
    {
		$formatted_phone = (substr($phone, 0, 1) == '0') ? '+62' . substr($phone, 1) : ((substr($phone, 0, 2) == '62') ? '+' . $phone : $phone);

        $this->attributes['phone'] = $formatted_phone;

        return $this;
    }
}