<?php

namespace LPI\Auth\Authorization;

/*
 * File: RemoteAuthorization.php
 * Project: LPI:Auth
 * File Created: Tuesday, 12th October 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 21st October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

class RemoteAuthorization
{
	protected $base_uri = "";

	public function __construct()
    {
        $this->base_uri = env("API_URI") ?? "https://api.linindo.com/v1/";
    }

	/**
	 * @var array|string|null
	 */
	protected $error;

	/**
	 * Returns any error(s) from the last call.
	 *
	 * @return array|string|null
	 */
	public function error()
	{
		return $this->error;
	}

	//--------------------------------------------------------------------
	// Actions
	//--------------------------------------------------------------------

	/**
	 * Checks to see if a user is in a group.
	 *
	 * Groups can be either a string, with the name of the group, an INT
	 * with the ID of the group, or an array of strings/ids that the
	 * user must belong to ONE of. (It's an OR check not an AND check)
	 *
	 * @param mixed $groups
	 * @param int $userId
	 *
	 * @return bool
	 */
	public function inGroup($groups, int $userId)
	{
		try
		{
			$client = \Config\Services::curlrequest(["base_uri" => $this->base_uri]);
			$response = $client->post("user/in-groups", [
				"json" => [
					"id" => $userId,
					"groups" => $groups,
				],
				"http_errors" => false
			]);

			$body = $response->getBody();
			$code = (int)$response->getStatusCode();
			if (strpos($response->getHeader("content-type"), "application/json") !== false)
			{
				$body = json_decode($body);
			}

			if ($code === 200 && $body)
			{
				return true;
			}
		}
		catch (\Exception $e)
		{
			$this->error = $e->getMessage();
		}

		return false;
	}

	/**
	 * Checks a user's groups to see if they have the specified permission.
	 *
	 * @param int|string $permission Permission ID or name
	 * @param int $userId
	 *
	 * @return mixed
	 */
	public function hasPermission($permission, int $userId)
	{
		try
		{
			$client = \Config\Services::curlrequest(["base_uri" => $this->base_uri]);
			$response = $client->post("user/has-permission", [
				"json" => [
					"id" => $userId,
					"permission" => $permission,
				],
				"http_errors" => false
			]);

			$body = $response->getBody();
			$code = (int)$response->getStatusCode();
			if (strpos($response->getHeader("content-type"), "application/json") !== false)
			{
				$body = json_decode($body);
			}

			if ($code === 200 && $body)
			{
				return true;
			}
		}
		catch (\Exception $e)
		{
			$this->error = $e->getMessage();
		}

		return false;
	}
}