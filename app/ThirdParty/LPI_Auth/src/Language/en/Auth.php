<?php

// override core en language system validation or define your own en language validation message
return [
    // Forgotten Passwords
    'forgotNoUsername' => 'Unable to locate a user with that NIK.',

    // Login views
    'forgotPassword' => 'Forgot Your Password?',
    'loginTitle' => 'Login to service',
    'username' => 'NIK',
    'enterCodeUsernamePassword' => 'Enter the code you received, your NIK, and your new password.',
    'enterNikForInstructions' => 'No problem! Enter your NIK below and we will send instructions to reset your password.',
];
