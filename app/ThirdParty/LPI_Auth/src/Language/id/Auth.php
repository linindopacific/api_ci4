<?php

// override core id language system validation or define your own id language validation message
return [

    // Exceptions
    'unknownError' => 'Maaf, kami mengalami masalah saat mengirim surel kepada Anda. Silakan coba lagi nanti.',

    // Activation
    'activationSuccess' => 'Silakan konfirmasi akun Anda dengan mengklik link aktivasi di surel yang kami kirimkan.',

    // Forgotten Passwords
    'forgotNoUser' => 'Tidak dapat menemukan pengguna dengan surel tersebut.',
    'forgotEmailSent' => 'Token keamanan telah dikirim ke surel Anda.  Masukkan ke dalam kotak di bawah untuk melanjutkan.',
    'errorEmailSent' => 'Tidak dapat mengirim surel dengan instruksi setel ulang kata sandi ke: {0}',
    'forgotNoUsername' => 'Tidak dapat menemukan pengguna dengan NIK tersebut.',

    // Passwords
    'suggestPasswordPersonal' => 'Variasi pada alamat surel atau nama pengguna Anda tidak boleh digunakan untuk kata sandi.',

    // Login views
    'forgotPassword' => 'Lupa kata sandi?',
    'loginTitle' => 'Masuk ke layanan',
    'enterEmailForInstructions' => 'Tidak masalah! Masukkan surel Anda di bawah ini dan kami akan mengirimkan instruksi untuk mengatur ulang kata sandi Anda.',
    'email' => 'Surel',
    'emailAddress' => 'Alamat Surel',
    'emailOrUsername' => 'Surel atau nama pengguna',
    'weNeverShare' => 'Kami tidak akan pernah membagi surel Anda dengan orang lain.',
    'enterCodeEmailPassword' => 'Masukkan kode yang Anda terima melalui surel, alamat surel Anda, dan kata sandi baru Anda.',
    'username' => 'NIK',
    'enterCodeUsernamePassword' => 'Masukkan kode yang Anda terima, NIK Anda, dan kata sandi baru Anda.',
    'enterNikForInstructions' => 'Tidak masalah! Masukkan NIK Anda di bawah ini dan kami akan mengirimkan instruksi untuk mengatur ulang kata sandi Anda.',
];
