<?php

namespace LPI\Auth\Controllers;

/*
 * File: AuthController.php
 * Project: LPI:Auth
 * File Created: Monday, 6th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Controller;
use CodeIgniter\Session\Session;
use LPI\Auth\Config\Auth as LPI_Auth;

class AuthController extends Controller
{
    protected $auth;

    /**
	 * @var Session
	 */
	protected $session;

	protected $base_uri = "";

	public function __construct()
	{
		// Most services in this controller require
		// the session to be started - so fire it up!
		$this->session = service('session');

		$this->auth = service('authentication', 'jwt');

		$this->base_uri = env("API_URI") ?? "https://api.linindo.com/v1/";
	}

    //--------------------------------------------------------------------
	// Login/out
	//--------------------------------------------------------------------

	/**
	 * Displays the login form, or redirects
	 * the user to their destination/home if
	 * they are already logged in.
	 */
	public function login()
	{
		// No need to show a login form if the user
		// is already logged in.
		if ($this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? site_url('/');
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

        // Set a return URL if none is specified
        $_SESSION['redirect_url'] = session('redirect_url') ?? previous_url() ?? site_url('/');

		return $this->_render('login');
	}

    /**
	 * Attempts to verify the user's credentials
	 * through a POST request.
	 */
	public function attemptLogin()
	{
		$rules = [
			'login'	=> 'required',
			'password' => 'required',
		];
		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		$login = $this->request->getPost('login');
		$password = $this->request->getPost('password');

		// Try to log them in...
		if (! $this->auth->attempt(['login' => $login, 'password' => $password]))
		{
			return redirect()->back()->withInput()->with('error', $this->auth->error() ?? lang('Auth.badAttempt'));
		}

		$redirectURL = session('redirect_url') ?? site_url('/');
		unset($_SESSION['redirect_url']);

		return redirect()->to($redirectURL)->withCookies()->with('message', lang('Auth.loginSuccess'));
	}

    //--------------------------------------------------------------------
	// Forgot Password
	//--------------------------------------------------------------------

	/**
	 * Displays the forgot password form.
	 */
	public function forgotPassword()
	{
		// No need to show a login form if the user
		// is already logged in.
		if ($this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? site_url('/');
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		return $this->_render('forgot');
	}

    /**
	 * Attempts to find a user account with that password
	 * and send password reset instructions to them.
	 */
	public function attemptForgot()
	{
		$rules = [
			'username'	=> [
				'label' => 'NIK',
				'rules' => 'required'
			]
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

        try
        {
            $client = \Config\Services::curlrequest(["base_uri" => $this->base_uri]);
            $response = $client->post('forgot', [
                'json' => [
                    'nik' => $this->request->getPost('username')
                ],
                'http_errors' => false,
				"verify" => false
            ]);

            $body = $response->getBody();
            $code = (int)$response->getStatusCode();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $body = json_decode($body);
            }

            if ($code === 200)
            {
                return redirect()->to(site_url(route_to('reset-password') .'?token='. $body->token))->with('message', lang('Auth.forgotEmailSent'));
            }
            else
            {
                return redirect()->back()->with('error', $body->messages->error);
            }
        }
        catch (\Exception $e)
        {
            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
	 * Displays the Reset Password form.
	 */
	public function resetPassword()
	{
		// No need to show a login form if the user
		// is already logged in.
		if ($this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? site_url('/');
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

		$token = $this->request->getGet('token');

		return $this->_render('reset', [
			'token'  => $token
		]);
	}

	/**
	 * Verifies the code with the email and saves the new password,
	 * if they all pass validation.
	 *
	 * @return mixed
	 */
	public function attemptReset()
	{
		$rules = [
			'token' => 'required',
			'username' => [
				'label' => 'NIK',
				'rules' => 'required'
			],
			'password' => 'required',
			'pass_confirm' => 'required|matches[password]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		try
		{
			$client = \Config\Services::curlrequest(["base_uri" => $this->base_uri]);
            $response = $client->post('reset-password', [
                'json' => [
                    'nik' => $this->request->getPost('username'),
					'token' => $this->request->getPost('token'),
					'password' => $this->request->getPost('password')
                ],
                'http_errors' => false,
				"verify" => false
            ]);

			$body = $response->getBody();
            $code = (int)$response->getStatusCode();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $body = json_decode($body, true);
            }

			if ($code >= 400)
            {
				return redirect()->back()->withInput()->with('errors', $body['messages']);
            }

		}
		catch (\Exception $e)
		{
			return redirect()->back()->with('error', $e->getMessage());
		}

		return redirect()->route('login')->with('message', lang('Auth.resetSuccess'));
	}

	/**
	 * Log the user out.
	 */
	public function logout()
	{
		if ($this->auth->check())
		{
			$this->auth->logout();
		}

		return redirect()->to(site_url('/'));
	}

    protected function _render(string $view, array $data = [])
	{
		$config = new LPI_Auth();

		$view = 'App\ThirdParty\LPI_Auth\src\Views\\' . $config->theme . '\\' . $view;

		return view($view, $data);
	}
}