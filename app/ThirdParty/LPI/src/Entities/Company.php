<?php namespace LPI\Entities;

/*
 * File: Company.php
 * Project: -
 * File Created: Tuesday, 3rd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 3rd August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;

class Company extends Entity
{
	protected $dates   = [
		'created_at',
		'updated_at',
		'deleted_at',
	];
}
