<?php namespace LPI\Entities;

/*
 * File: Coa.php
 * Project: Entities
 * File Created: Monday, 16th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;

class Coa extends Entity
{
    protected $dates   = [
		'created_at',
		'updated_at',
		'deleted_at',
	];
}