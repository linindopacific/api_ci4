<?php namespace LPI\Controllers;

/*
 * File: CustomerLedgerEntryController.php
 * Project: LPI
 * File Created: Wednesday, 25th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 26th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Controllers\BaseController;
use LPI\Models\CompanyModel;
use LPI\Models\NAV\CustomerLedgerEntryModel;

class CustomerLedgerEntryController extends BaseController
{
    /**
     * @api {get} /cash/receivable Get receivable
     * @apiDescription Get receivable from vendor entries.
     * @apiName GetReceivable
     * @apiGroup LedgerEntry
	 * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiQuery {String} entity Mandatory entity.
     * @apiQuery {String} year Optional posting date of year.
     *
     * @apiSampleRequest off
     */
    public function getReceivable()
    {
        $entity = $this->request->getGet('entity');
        $year = $this->request->getGet('year');

        $validation = service('validation');
        $validation->setRules([
            'entity' => 'required|is_valid_entity',
            'year' => 'permit_empty|string'
        ]);

        if (! $validation->run(['entity' => $entity, 'year' => $year]))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

		try
        {
            $db = db_connect('nav');
            $companyModel = new CompanyModel();
			$company = $companyModel->where('prefix', $entity)->first();

			$model = new CustomerLedgerEntryModel($db, $company);
			$data = $model->getReceivable($year);
		}
        catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
        {
            $message = preg_replace('/\s+/', ' ', $e->getMessage());
            return $this->failServerError($message, $e->getCode());
        }
        catch (\Exception $e)
        {
			return $this->fail($e->getMessage());
		}

        return $this->respond($data);
    }
}