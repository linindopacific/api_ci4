<?php namespace LPI\Controllers;

/*
 * File: CompanyController.php
 * Project: LPI
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 12th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Controllers\BaseController;
use LPI\Entities\Company;
use LPI\Models\CompanyModel;
use LPI\Models\NAV\CompanyModel as NavCompanyModel;

class CompanyController extends BaseController
{
    /**
     * @api {post} /company Create new company
     * @apiDescription Create a new company on AERPS system.
     * @apiName PostCompany
     * @apiGroup Company
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam (Json) {String} name Mandatory company name.
     * @apiParam (Json) {String} prefix Mandatory company unique prefix.
     * @apiParam (Json) {String} short Mandatory company unique short.
     * @apiParam (Json) {String} table Mandatory company table.
     * @apiParam (Json) {String} group Mandatory company group.
     * @apiParam (Json) {String} priority Optional company priority.
     *
     * @apiSampleRequest off
     */
    public function createCompany()
    {
        $db = db_connect('default');
        $model = new CompanyModel($db);

        if (! $this->validate($model->getValidationRules()))
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }

        try
        {
            $company = new Company($this->request->getJSON(true));
            $company->priority = $model->get_priority();

            if(! $model->save($company))
            {
                return $this->fail($model->errors());
            }
        }
        catch (\Exception $e)
        {
            return $this->failServerError($e->getMessage());
        }

        return $this->respondCreated($company);
    }

    /**
     * @api {put} /company/:id Update company
     * @apiDescription Update company info.
     * @apiName PutCompany
     * @apiGroup Company
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Mandatory companies unique ID.
     * @apiParam (Json) {String} name Company name.
     * @apiParam (Json) {String} prefix Company unique prefix.
     * @apiParam (Json) {String} short Company unique short.
     * @apiParam (Json) {String} table Company table.
     * @apiParam (Json) {String} group Company group.
     * @apiParam (Json) {String} priority Company priority.
	 *
     * @apiSampleRequest off
     */
	public function updateCompany($id)
	{
		$model = new CompanyModel();

		$company = $model->find($id);
		if (empty($company))
		{
			return $this->failNotFound();
		}
		else
		{
			$data = $this->request->getJSON(true);
			if ($data)
			{
				foreach ($data as $key => $value)
				{
					try
					{
						is_null($model->findColumn($key));
						$company->$key = $value;
					}
					catch (\Exception $e)
					{
						return $this->fail($e->getMessage());
					}
				}

				if ($company->hasChanged())
				{
					if (! $this->validate([
						'name' => 'permit_empty|alpha_numeric_punct|max_length[50]',
                        'prefix' => 'permit_empty|alpha|max_length[4]|is_unique[companies.prefix,id,{id}]',
                        'short' => 'permit_empty|alpha|max_length[1]|is_unique[companies.short,id,{id}]',
                        'table' => 'permit_empty|alpha_numeric_punct|max_length[50]',
                        'group' => 'permit_empty|integer|max_length[4]',
                        'priority' => 'permit_empty|integer|max_length[4]',
					]))
					{
						return $this->failValidationErrors($this->validator->getErrors());
					}

					try
					{
						if (! $model->save($company))
						{
							return $this->fail($model->errors());
						}
					}
					catch (\Exception $e)
					{
						return $this->failServerError($e->getMessage());
					}

					return $this->respondUpdated($company);
				}

				return $this->respond([]);
			}

			return $this->respondNoContent();
		}
	}

    /**
     * @api {get} /company/:id Get companies
     * @apiDescription Get companies from AERPS system.
     * @apiName GetCompany
     * @apiGroup Company
	 * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam {Number} id Optional companies unique ID.
     * @apiParam (Json) {String} name Optional companies name.
     * @apiParam (Json) {String} prefix Optional companies unique prefix.
     * @apiParam (Json) {String} short Optional companies unique short.
     * @apiParam (Json) {String} table Optional companies table.
     * @apiParam (Json) {String} group Optional companies group.
     * @apiParam (Json) {String} priority Optional companies priority.
     * @apiParamExample {json} Request-Example:
     *      {
     *          "name" : "Linindo Pacific International",
     *          "prefix" : "LPI",
     *          "short" : "L"
     *      }
     *
     * @apiSampleRequest off
     */
    public function getCompany($id = NULL)
    {
        $db = db_connect('default');
        $model = new CompanyModel($db);

        $data = $this->request->getJSON(true);
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                try {
                    is_null($model->findColumn($key));
                    $model->where($key, $value);
                } catch (\Exception $e) {
                    return $this->fail($e->getMessage());
                }
            }
        }

        $response  = $id ? $model->where('id', $id)->findAll() : $model->findAll();

        return $this->respond($response);
    }

    /**
     * @api {delete} /company/:id Delete company
     * @apiDescription Delete company info.
     * @apiName DeleteCompany
     * @apiGroup Company
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Mandatory companies unique ID.
	 *
     * @apiSampleRequest off
     */
	public function deleteCompany($id)
	{
		$model = new CompanyModel();

        $company = $model->find($id);
		if (empty($company))
		{
			return $this->failNotFound();
		}
		else
		{
			if (! $model->delete($id))
			{
				return $this->fail($model->errors());
			}
		}

		return $this->respondDeleted([]);
	}

    /**
     * @api {get} /company-nav/:name Get nav companies
     * @apiDescription Get companies from DynamicsNAV system.
     * @apiName GetNavCompany
     * @apiGroup Company
	 * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam {String} name Mandatory companies name.
     *
     * @apiSampleRequest off
     */
    public function getCompanyNav($name = NULL)
    {
        try
        {
            $db = db_connect('nav');
            $model = new NavCompanyModel($db);

            $response = $model->get($name);
            return $this->respond($response);
        }
        catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
        {
            $message = preg_replace('/\s+/', ' ', $e->getMessage());
            return $this->failServerError($message, $e->getCode());
        }
    }
}