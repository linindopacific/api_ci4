<?php namespace LPI\Controllers;

/*
 * File: LogbookController.php
 * Project: LPI
 * File Created: Monday, 25th October 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 26th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Controllers\BaseController;
use LPI\Models\LogbookModel;

class LogbookController extends BaseController
{
    /**
     * @api {get} /logbook/:id Get logbook car
     * @apiDescription Get car from logbook system.
     * @apiName GetLogbookCar
     * @apiGroup Logbook
	 * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam {Number} id Optional logbook car unique ID.
     * @apiSampleRequest off
     */
    public function getCar($id = NULL)
    {
        $db = db_connect("logbook");
        $model = new LogbookModel($db);
        $model->setTable("v_cars");

        $data = $this->request->getJSON(true);
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                try {
                    is_null($model->findColumn($key));
                    $model->where($key, $value);
                } catch (\Exception $e) {
                    return $this->fail($e->getMessage());
                }
            }
        }

        $response  = $id ? $model->where("id", $id)->findAll() : $model->findAll();

        return $this->respond($response);
    }
}