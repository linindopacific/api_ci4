<?php namespace LPI\Controllers;

/*
 * File: ItemLedgerEntryController.php
 * Project: LPI
 * File Created: Thursday, 26th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 26th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Controllers\BaseController;
use LPI\Models\CompanyModel;
use LPI\Models\NAV\ItemLedgerEntryModel;

class ItemLedgerEntryController extends BaseController
{
    /**
     * @api {get} /inventory/stock Get soh
     * @apiDescription Get stock on hand from ledger entries.
     * @apiName GetSOH
     * @apiGroup LedgerEntry
	 * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam (Json) {String} entity Mandatory entity.
     * @apiParam (Json) {String} itemNo Mandatory item candelux no.
     * @apiParam (Json) {String} locationCode Optional location code.
     *
     * @apiSampleRequest off
     */
    public function getSOH()
    {
        $postData = $this->request->getJSON(true);
        $entity = $postData['entity'] ?? '';
        $itemNo = $postData['itemNo'] ?? '';
        $locationCode = $postData['locationCode'] ?? '';

        $validation = service('validation');
        $validation->setRules([
            'entity' => 'required|is_valid_entity',
            'itemNo' => 'required|alpha_numeric|exact_length[8]',
            'locationCode' => 'permit_empty'
        ]);

        if (! $validation->run(['entity' => $entity, 'itemNo' => $itemNo, 'locationCode' => $locationCode]))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

		try
        {
            $db = db_connect('nav');
            $companyModel = new CompanyModel();
			$company = $companyModel->where('prefix', $entity)->first();

			$model = new ItemLedgerEntryModel($db, $company);
			$data = $model->getSOH($itemNo, $locationCode);
		}
        catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
        {
            $message = preg_replace('/\s+/', ' ', $e->getMessage());
            return $this->failServerError($message, $e->getCode());
        }
        catch (\Exception $e)
        {
			return $this->fail($e->getMessage());
		}

        return $this->respond($data);
    }
}