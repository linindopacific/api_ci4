<?php namespace LPI\Controllers;

/*
 * File: BaseController.php
 * Project: LPI
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 10th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;

class BaseController extends Controller
{
    use ResponseTrait;

    public function __construct()
    {

    }
}