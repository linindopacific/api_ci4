<?php namespace LPI\Controllers;

/*
 * File: UserController.php
 * Project: LPI
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 12th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Auth\Entities\User;
use LPI\Auth\Models\UserModel;
use LPI\Controllers\BaseController;
use LPI\Models\NAV\UserModel as NavUserModel;

class UserController extends BaseController
{
    /**
     * @api {post} /user Create new user
     * @apiDescription Create a new user on AERPS system.
     * @apiName PostUser
     * @apiGroup User
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam (Json) {Number} nik Mandatory users NIK.
     * @apiParam (Json) {String} email Mandatory users email address.
     * @apiParam (Json) {String} password Mandatory users password.
     * @apiParam (Json) {String} firstname Optional users first name.
     * @apiParam (Json) {String} lastname Optional users last name.
     * @apiParam (Json) {String} phone Optional users phone number.
     *
     * @apiSampleRequest off
     */
    public function createUser()
    {
        $config = config("Auth");
        $db = db_connect("default");
        $model = new UserModel($db);

        if (! $this->validate([
			"nik" => "required|alpha_numeric_space|min_length[1]|max_length[30]|is_unique[users.username,id,{id}]",
			"email" => "required|valid_email|is_unique[users.email,id,{id}]",
            "password" => "required|strong_password",
            "firstname" => "permit_empty|alpha_space|max_length[63]",
            "lastname" => "permit_empty|alpha_space|max_length[63]",
            "phone" => "permit_empty|is_natural|min_length[10]|max_length[20]"
		]) )
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }

        try
        {
            $user = new User($this->request->getJSON(true));

            $config->requireActivation === null ? $user->activate() : $user->generateActivateHash();

            // Ensure default group gets assigned if set
            if (! empty($config->defaultUserGroup)) {
                $model = $model->withGroup($config->defaultUserGroup);
            }

            // assign random uid
            $user->uid = $model->get_unused_id();

            if(! $model->save($user))
            {
                return $this->fail($model->errors());
            }
        }
        catch (\Exception $e)
        {
            return $this->failServerError($e->getMessage());
        }

        return $this->respondCreated($user);
    }

    /**
     * @api {put} /user/:id Update user
     * @apiDescription Update user info.
     * @apiName PutUser
     * @apiGroup User
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
     * @apiParam {Number} id Mandatory users unique ID.
     * @apiParam (Json) {Number} uid Users aerps unique ID.
     * @apiParam (Json) {Number} nik Users NIK.
     * @apiParam (Json) {String} email Users email address.
     * @apiParam (Json) {String} firstname Users first name.
     * @apiParam (Json) {String} lastname Users last name.
     * @apiParam (Json) {String} phone Users phone number.
	 *
     * @apiSampleRequest off
     */
	public function updateUser($id)
	{
        $db = db_connect("default");
		$model = new UserModel($db);

		$users = $model->find($id);
		if (empty($users))
		{
			return $this->failNotFound();
		}
		else
		{
			$data = $this->request->getJSON(true);
			if ($data)
			{
				foreach ($data as $key => $value)
				{
                    $key = ($key == "nik") ? "username" : $key;
					try
					{
						is_null($model->findColumn($key));
						$users->$key = $value;
					}
					catch (\Exception $e)
					{
						return $this->fail($e->getMessage());
					}
				}

				if ($users->hasChanged())
				{
					if (! $this->validate([
						"uid" => "permit_empty|integer|is_unique[users.uid,id,{id}]",
                        "nik" => "permit_empty|alpha_numeric_punct|min_length[3]|max_length[30]|is_unique[users.username,id,{id}]",
						"email" => "permit_empty|valid_email|is_unique[users.email,id,{id}]",
						"firstname" => "permit_empty|alpha_space|max_length[63]",
						"lastname" => "permit_empty|alpha_space|max_length[63]",
						"phone" => "permit_empty|is_natural|min_length[10]|max_length[20]"
					]))
					{
						return $this->failValidationErrors($this->validator->getErrors());
					}

					try
					{
						if (! $model->save($users))
						{
							return $this->fail($model->errors());
						}
					}
					catch (\Exception $e)
					{
						return $this->failServerError($e->getMessage());
					}

					return $this->respondUpdated($users);
				}

				return $this->respond([]);
			}

			return $this->respondNoContent();
		}
	}

    /**
     * @api {get} /user/:id Get users
     * @apiDescription Get users from AERPS system.
     * @apiName GetUser
     * @apiGroup User
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam {Number} id Optional users unique ID.
     * @apiParam (Json) {Number} uid Optional users aerps unique ID.
     * @apiParam (Json) {Number} nik Optional users NIK.
     * @apiParam (Json) {String} email Optional users email address.
     * @apiParamExample {json} Request-Example:
     *      {
     *          "uid" : 1,
     *          "nik" : 999,
     *          "email" : "my@domain.com"
     *      }
     *
     * @apiSampleRequest off
     */
	public function getUser($id = NULL)
    {
        $db = db_connect("default");
        $model = new UserModel($db);

        $data = $this->request->getJSON(true);
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $key = ($key == "nik") ? "username" : $key;
                try {
                    is_null($model->findColumn($key));
                    $model->where($key, $value);
                } catch (\Exception $e) {
                    return $this->fail($e->getMessage());
                }
            }
        }

        $response  = $id ? $model->where("id", $id)->findAll() : $model->findAll();

        return $this->respond($response);
    }

    /**
     * @api {delete} /user/:id Delete user
     * @apiDescription Delete user info.
     * @apiName DeleteUser
     * @apiGroup User
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Mandatory users unique ID.
	 *
     * @apiSampleRequest off
     */
	public function deleteUser($id)
	{
        $db = db_connect("default");
		$model = new UserModel($db);

        $user = $model->find($id);
		if (empty($user))
		{
			return $this->failNotFound();
		}
		else
		{
			if (! $model->delete($id))
			{
				return $this->fail($model->errors());
			}
		}

		return $this->respondDeleted([]);
	}

    /**
     * @api {post} /user/:id Ban user
     * @apiDescription Banned user from authentication.
     * @apiName BanUser
     * @apiGroup User
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Mandatory users unique ID.
	 * @apiParam (Json) {String} reason=resign Optional banned reason.
	 *
     * @apiSampleRequest off
     */
	public function banUser($id)
	{
        $db = db_connect("default");
		$model = new UserModel($db);

        $user = $model->find($id);
		if (empty($user))
		{
			return $this->failNotFound();
		}
		else
		{
            try
            {
                $reason = "resign";
                $body = $this->request->getJSON( true);
                if ($body)
                {
                    $reason = ($body["reason"]) ?? $reason;
                }
                $user->ban($reason);
                if (! $model->save($user))
                {
                    return $this->fail($model->errors());
                }
            }
            catch (\Exception $e)
            {
                return $this->fail($e->getMessage());
            }

		}

		return $this->respondDeleted([]);
	}

    /**
     * @api {get} /user-nav/:username Get nav users
     * @apiDescription Get users from DynamicsNAV system.
     * @apiName GetNavUser
     * @apiGroup User
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiParam {String} username Mandatory users NAV ID.
     *
     * @apiSampleRequest off
     */
    public function getUserNav($username = NULL)
    {
        try
        {
            $db = db_connect("nav");
            $model = new NavUserModel($db);

            $response  = $model->get($username);
            return $this->respond($response);
        }
        catch (\CodeIgniter\Database\Exceptions\DatabaseException $e)
        {
            $message = preg_replace("/\s+/", " ", $e->getMessage());
            return $this->failServerError($message, $e->getCode());
        }
    }

    public function inGroups()
    {
        $db = db_connect("default");
        $model = new UserModel($db);

        $data = $this->request->getJSON(true);

        $validation =  \Config\Services::validation();
        $validation->setRules([
			"id" => "required|numeric",
            "groups" => "required",
		]);

        if (! $validation->run($data))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

        $user = $model->find($data["id"]);
        if (! empty($user))
        {
            $userId = $user->id;
            $groups = $data["groups"];

            $authorize = service("authorization", null, null, new UserModel());
            if ($authorize->inGroup($groups, $userId))
            {
                return $this->respond(true);
            }
            else
            {
                return $this->respond(false);
            }
        }

        return $this->failNotFound();
    }

    public function hasPermission()
    {
        $db = db_connect("default");
        $model = new UserModel($db);

        $data = $this->request->getJSON(true);

        $validation =  \Config\Services::validation();
        $validation->setRules([
			"id" => "required|numeric",
            "permission" => "required",
		]);

        if (! $validation->run($data))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

        $user = $model->find($data["id"]);
        if (! empty($user))
        {
            $userId = $user->id;
            $permission = $data["permission"];

            $authorize = service("authorization", null, null, new UserModel());
            if ($authorize->hasPermission($permission, $userId) ?? false)
            {
                return $this->respond(true);
            }
            else
            {
                return $this->respond(false);
            }
        }

        return $this->failNotFound();
    }
}