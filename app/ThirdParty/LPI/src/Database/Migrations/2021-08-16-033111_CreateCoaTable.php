<?php namespace LPI\Database\Migrations;

/*
 * File: 2021-08-16-033111_CreateCoaTable.php
 * Project: -
 * File Created: Monday, 16th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class CreateCoaTable extends Migration
{
	protected $DBGroup = 'finance';

	protected $table = 'coas';

	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'unsigned' => true,
				'auto_increment' => true
			],
			'category' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => false
			],
			'no' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'unique' => true,
				'null' => false
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => false
			],
			'restricted' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 1
			],
			'pinned' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
			'updated_at' => [
				'type' => 'datetime',
				'null' => true
			],
			'deleted_at' => [
				'type' => 'datetime',
				'null' => true
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable($this->table);
	}

	public function down()
	{
		// backup first
		$this->_backup();

		$this->forge->dropTable($this->table);
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper('filesystem');
		$filename = $this->table . '_' . time() . '.csv';
		if (! write_file(WRITEPATH . 'dbdump/' . $filename, $data))
		{
			log_message('error', 'Unable to write the backup file');
			die('Unable to write the backup file');
		}
	}
}