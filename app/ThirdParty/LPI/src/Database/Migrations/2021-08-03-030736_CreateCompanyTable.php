<?php namespace LPI\Database\Migrations;

/*
 * File: 2021-08-03-030736_CreateCompanyTable.php
 * Project: -
 * File Created: Tuesday, 3rd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class CreateCompanyTable extends Migration
{
	protected $table = 'companies';

	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'unsigned' => true,
				'auto_increment' => true
			],
			'name' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false
			],
			'prefix' => [
				'type' => 'VARCHAR',
				'constraint' => '4',
				'unique' => true,
				'null' => false
			],
			'short' => [
				'type' => 'VARCHAR',
				'constraint' => '1',
				'unique' => true,
				'null' => false
			],
			'table' => [
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => false
			],
			'group' => [
				'type' => 'TINYINT',
				'null' => false
			],
			'priority' => [
				'type' => 'TINYINT',
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'deleted_at' => [
				'type' => 'datetime',
				'null' => true
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable($this->table);
	}

	public function down()
	{
		// backup first
		$this->_backup();

		$this->forge->dropTable($this->table);
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper('filesystem');
		$filename = $this->table . '_' . time() . '.csv';
		if (! write_file(WRITEPATH . 'dbdump/' . $filename, $data))
		{
			log_message('error', 'Unable to write the backup file');
			die('Unable to write the backup file');
		}
	}
}
