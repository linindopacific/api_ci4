<?php namespace LPI\Models;

/*
 * File: CompanyModel.php
 * Project: LPI
 * File Created: Tuesday, 3rd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 25th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Model;
use LPI\Entities\Company;

class CompanyModel extends Model
{
	protected $table = 'companies';

	protected $returnType = Company::class;

	protected $allowedFields = [
		'name', 'prefix', 'short', 'table', 'group', 'priority'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
        'name' => 'required|alpha_numeric_punct|max_length[50]',
        'prefix' => 'required|alpha|max_length[4]|is_unique[companies.prefix,id,{id}]',
        'short' => 'required|alpha|max_length[1]|is_unique[companies.short,id,{id}]',
        'table' => 'required|alpha_numeric_punct|max_length[50]',
        'group' => 'required|integer|max_length[4]',
        'priority' => 'permit_empty|integer|max_length[4]',
    ];

    protected $validationMessages = [];

    /**
     * Get next priority
     *
     * @return int
     */
    public function get_priority()
    {
        $query = $this->select('priority')
                      ->orderBy('priority', 'DESC')
                      ->first();

        if ( empty($query) ) {
            return 1;
        }

        return $query->priority + 1;
    }
}