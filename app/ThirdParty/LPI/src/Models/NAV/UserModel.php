<?php namespace LPI\Models\NAV;

/*
 * File: UserModel.php
 * Project: LPI
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 3rd August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\ConnectionInterface;

class UserModel
{
    protected $db;

    protected $table = 'User';

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function get(string $username = null)
    {
        $binds = [];

        $sql = "SELECT [User Name] AS userName, [Full Name] AS fullName, CASE WHEN [State] = 0 THEN 'Enabled' ELSE 'Disabled' END AS state, CASE WHEN [License Type] = 0 THEN 'Full' ELSE 'Limited' END AS licenseType, [Contact Email] AS contactEmail ";
        $sql.= "FROM [" . $this->table . "] ";
        if ($username) {
            $name = "CORPGROUP\\" . strtoupper($username);
            $binds[] = $name;
            $sql.= "WHERE [User Name] = ? ";
        }
        $sql.= "ORDER BY [User Name]";

        $data = $this->db->query($sql, $binds);

        return $data->getResultArray();
    }
}