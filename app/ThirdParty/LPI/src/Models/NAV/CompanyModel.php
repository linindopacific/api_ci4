<?php namespace LPI\Models\NAV;

/*
 * File: CompanyModel.php
 * Project: LPI
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 2nd August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\ConnectionInterface;

class CompanyModel
{
    protected $db;

    protected $table = 'Company';

    public function __construct(ConnectionInterface &$db)
    {
        $this->db =& $db;
    }

    public function get(string $name = null)
    {
        $binds = [];

        $sql = "SELECT [Name] AS name FROM [" . $this->table . "] ";
        if ($name) {
            $binds[] = $name;
            $sql.= "WHERE [Name] = ? ";
        }
        $sql.= "ORDER BY [Name]";

        $data = $this->db->query($sql, $binds);

        return $data->getResultArray();
    }
}