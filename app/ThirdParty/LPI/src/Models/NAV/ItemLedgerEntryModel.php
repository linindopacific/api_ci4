<?php namespace LPI\Models\NAV;

/*
 * File: ItemLedgerEntryModel.php
 * Project: LPI
 * File Created: Wednesday, 25th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 26th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Models\NAV\NavModel;

class ItemLedgerEntryModel extends NavModel
{
    protected $table = 'Item Ledger Entry';

    public function getBin(string $itemNo, string $locationCode)
    {
        $sql = "SELECT [Bin Code] as binCode, SUM([Qty_ (Base)]) AS qtyBase ";
        $sql.= "FROM [" . $this->companyTable . "Warehouse Entry] ";
        $sql.= "WHERE [Item No_] = ? AND [Location Code] = ? ";
        $sql.= "GROUP BY [Bin Code] ";
        $sql.= "HAVING SUM([Qty_ (Base)]) <> 0 ";

        $binds = [$itemNo, $locationCode];

        $query = $this->db->query($sql, $binds);

        return $query->getResultArray();
    }

    public function getSOH(string $itemNo, string $locationCode = NULL)
    {
        $cacheName = 'soh-' . $this->companyPrefix . '_' . $itemNo . '_' . $locationCode;
        if (! $response = cache($cacheName))
        {
            $binds = [$itemNo, $itemNo];

            $sql = "SELECT a.[Location Code] as locationCode, ISNULL(SUM(a.[Quantity]),0) AS qtyBase, ";
            $sql.= "(SELECT TOP 1 [Base Unit of Measure] FROM [" . $this->companyTable . "Item] WHERE [No_] = ?) as baseUom ";
            $sql.= "FROM [" . $this->companyTable . $this->table . "] a ";
            $sql.= "WHERE a.[Item No_] = ? ";
            if ($locationCode)
            {
                $binds[] = $locationCode;
                $sql.= "AND a.[Location Code] = ? ";
            }
            $sql.= "GROUP BY a.[Location Code] ";
            $sql.= "HAVING SUM(a.[Quantity]) <> 0 ";

            $query = $this->db->query($sql, $binds);

            $result = $query->getResultArray();

            $response = [
                'baseUom' => $result[0]['baseUom'],
                'qtyBase' => 0
            ];

            for ($i=0; $i < count($result); $i++) {
                foreach ($result[$i] as $key => $value) {
                    $value = ($key === 'qtyBase') ? floatval($value) : $value;
                    if ($key === 'qtyBase')
                    {
                        $response['qtyBase'] += $value;
                    }

                    if ($key <> 'baseUom') $response['data'][$i][$key] = $value;
                }

                $bins = $this->getBin($itemNo, $response['data'][$i]['locationCode']);
                $response['data'][$i]['bins'] = [];
                for ($j=0; $j < count($bins); $j++) {
                    foreach ($bins[$j] as $key2 => $value2) {
                        $value2 = ($key2 === 'qtyBase') ? floatval($value2) : $value2;
                        $response['data'][$i]['bins'][$j][$key2] = $value2;
                    }
                }
            }

            // Save into the cache for 60 seconds
            cache()->save($cacheName, $response);
        }

        return $response;
    }

}