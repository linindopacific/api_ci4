<?php namespace LPI\Models\NAV;

/*
 * File: NavModel.php
 * Project: LPI
 * File Created: Wednesday, 25th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Entities\Company;
use CodeIgniter\Database\ConnectionInterface;

class NavModel
{
    protected $db;

    protected $companyPrefix;

    protected $companyTable;

    protected $table;

    public function __construct(ConnectionInterface &$db, Company $company)
    {
        $this->db =& $db;

        $this->companyPrefix = $company->prefix;

        $this->companyTable = $company->table;
    }
}