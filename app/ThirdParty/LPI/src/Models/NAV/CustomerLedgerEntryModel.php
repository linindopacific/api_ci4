<?php namespace LPI\Models\NAV;

/*
 * File: CustomerLedgerEntryModel.php
 * Project: LPI
 * File Created: Wednesday, 25th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Models\NAV\NavModel;

class CustomerLedgerEntryModel extends NavModel
{
    protected $table = 'Cust_ Ledger Entry';

    public function getReceivable(string $year = NULL)
    {
        $cacheName = 'entry-receivable-' . $this->companyPrefix . '_' . $year;
        if (! $result = cache($cacheName))
        {
            $binds = [0, 0, 2];

            $sql = "SELECT CONVERT(VARCHAR, a.[Posting Date], 23) as postingDate, a.[Document No_] AS invoiceNo, CONVERT(Date, a.[Due Date], 105) AS dueDate, a.[Description] as description, (SELECT Name FROM [".$this->companyTable."Customer] WHERE No_ = a.[Sell-to Customer No_] ) AS customerName, ";
            $sql.= "STUFF((SELECT ', ' + [Document No_] FROM [".$this->companyTable."Detailed Cust_ Ledg_ Entry] WHERE [Cust_ Ledger Entry No_] = a.[Entry No_] AND LEFT([Document No_], 2) IN ('BR','CR','GJ') AND [Unapplied] = ? ORDER BY [Posting Date] FOR XML PATH('')), 1, 1, '' ) AS paymentNos, ";
            $sql.= "STUFF((SELECT ', ' + CONVERT(VARCHAR, [Posting Date], 23) FROM [".$this->companyTable."Detailed Cust_ Ledg_ Entry] WHERE [Cust_ Ledger Entry No_] = a.[Entry No_] AND LEFT([Document No_], 2) IN ('BR','CR','GJ') AND [Unapplied] = ? ORDER BY [Posting Date] FOR XML PATH('')), 1, 1, '' ) AS paymentDates ";
            $sql.= "FROM [".$this->companyTable.$this->table."] a ";
            $sql.= "WHERE a.[Document Type] = ? ";
            if ($year)
            {
                $binds[] = $year;
                $sql.= "AND YEAR(a.[Posting Date]) = ? ";
            }
            $sql.= "ORDER BY invoiceNo";

            $query = $this->db->query($sql, $binds);

            $result = $query->getResultArray();

            // Save into the cache for 60 minutes
            cache()->save($cacheName, $result, 3600);
        }

        return $result;
    }
}