<?php namespace LPI\Models;

/*
 * File: CoaModel.php
 * Project: LPI
 * File Created: Monday, 16th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 25th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Model;
use LPI\Entities\Coa;

class CoaModel extends Model
{
    protected $DBGroup = 'finance';

    protected $table = 'coas';

	protected $returnType = Coa::class;

	protected $allowedFields = [
		'id', 'category', 'no', 'description', 'restricted', 'pinned'
	];

	protected $useTimestamps = true;

    protected $useSoftDeletes = true;

	protected $validationRules = [
        'category' => 'required|max_length[10]',
        'no' => 'required|exact_length[15]|is_unique[coas.no,id,{id}]',
        'description' => 'required|max_length[255]',
        'restricted' => 'permit_empty|integer|max_length[1]',
        'pinned' => 'permit_empty|integer|max_length[1]',
    ];

    protected $validationMessages = [];

}