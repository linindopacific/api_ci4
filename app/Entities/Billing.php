<?php namespace App\Entities;

/*
 * File: Billing.php
 * Project: -
 * File Created: Monday, 9th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 10th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class Billing extends Entity
{
	protected $dates = [
		'expired_at',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function setExpiredAt(string $dateString)
    {
		$myTime = new Time($dateString);
        $this->attributes['expired_at'] = $myTime->toDateString();

        return $this;
    }

    public function getExpiredAt(string $format = 'Y-m-d')
    {
        // Convert to CodeIgniter\I18n\Time object
        $this->attributes['expired_at'] = $this->mutateDate($this->attributes['expired_at']);

        $timezone = $this->timezone ?? app_timezone();

        $this->attributes['expired_at']->setTimezone($timezone);

        return $this->attributes['expired_at']->format($format);
    }
}