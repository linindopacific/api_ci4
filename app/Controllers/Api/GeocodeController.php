<?php namespace App\Controllers\Api;

/*
 * File: GeocodeController.php
 * Project: -
 * File Created: Wednesday, 4th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 21st October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\Api\BaseApiController;

class GeocodeController extends BaseApiController
{
    /**
     * @api {get} /geocode-reverse Reverse geocode
     * @apiDescription Read full address from reversing geocode position.
     * @apiName GetLocation
     * @apiGroup Location
     * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiQuery {String="google","here","locationiq"} provider Mandatory Provider.
     * @apiQuery {String} latlng Mandatory Latitude Longitude.
     *
     * @apiSuccess {String} street Address
     * @apiSuccess {String} subdistrict Administrative area level 4
     * @apiSuccess {String} district Administrative area level 3
     * @apiSuccess {String} city Administrative area level 2
     * @apiSuccess {String} county Administrative area level 1
     * @apiSuccess {String} country Country
     *
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     *      "street": "Jl. MH. Thamrin, RT.007/RW.002, Cikokol, Kec. Tangerang, Kota Tangerang, Banten 15117, Indonesia",
     *      "subdistrict": "Cikokol",
     *      "district": "Kec. Tangerang",
     *      "city": "Kota Tangerang",
     *      "county": "Banten",
     *      "country": "Indonesia"
     *  }
     *
     * @apiSampleRequest off
     */
    public function getReverse()
    {
        $provider = $this->request->getGet('provider');
        $latlng = $this->request->getGet('latlng');

        $validation = service('validation');
        $validation->setRules([
            'provider' => 'required|in_list[google,here,locationiq]',
            'latlng' => ['label' => 'latitude longitude', 'rules' => 'required'],
        ]);

        if (! $validation->run(['provider' => $provider, 'latlng' => $latlng]))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

        $data = array(
			'street' => null,
			'subdistrict' => null,
			'district' => null,
			'city' => null,
			'county' => null,
			'country' => null
		);

        $_latlng = explode(',', $latlng, 2);
        $_lat = (float) $_latlng[0];
        $_lng = (float) $_latlng[1];
        $latlng = strval($_lat) . ',' . strval($_lng);

        if ($provider === 'google')
        {
            return $this->_reverse_google($data, $latlng);
        }
        elseif ($provider === 'here')
        {
            return $this->_reverse_here($data, $latlng);
        }
        elseif ($provider === 'locationiq')
        {
            return $this->_reverse_locationiq($data, $latlng);
        }
    }

    /**
     * Reverse geocoding using google.com
     *
     * @param array $data
     * @param string $latlng
     * @return string
     *
     */
    private function _reverse_google(array $data, string $latlng)
    {
        $cacheName = 'reverse-google_' . $latlng;
        if (! $result = cache($cacheName))
        {
            $client = \Config\Services::curlrequest(['base_uri' => 'https://maps.googleapis.com/maps/api/']);
            $response = $client->request('get', 'geocode/json', [
                'query' => [
                    'latlng' => $latlng,
                    'result_type' => 'route|administrative_area_level_4|administrative_area_level_3|administrative_area_level_2|administrative_area_level_1|country',
                    'language' => 'id',
                    'key' => env('GOOGLE_API_KEY')
                ],
                'http_errors' => false
            ]);

            $body = $response->getBody();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $result = json_decode($body, true);
            }

            if ($result['status'] === 'OK')
            {
                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
            else
            {
                return $this->fail($result['status']);
            }
        }

        $results = $result['results'];
        if (count($results) > 0)
        {
            for ($i=0; $i < count($results); $i++)
            {
                $formatted_address = null;
                foreach ($results[$i] as $key => $value)
                {
                    $formatted_address = ($key == 'formatted_address') ? $value : $formatted_address;
                    if ($key == 'types')
                    {
                        $data['street'] = ($value[0] == 'route') ? $formatted_address : $data['street'];
                        $data['subdistrict'] = ($value[0] == 'administrative_area_level_4') ? explode(',', $formatted_address, 5)[0] : $data['subdistrict'];
                        $data['district'] = ($value[0] == 'administrative_area_level_3') ? explode(',', $formatted_address, 4)[0] : $data['district'];
                        $data['city'] = ($value[0] == 'administrative_area_level_2') ? explode(',', $formatted_address, 3)[0] : $data['city'];
                        $data['county'] = ($value[0] == 'administrative_area_level_1') ? explode(',', $formatted_address, 2)[0] : $data['county'];
                        $data['country'] = ($value[0] == 'country') ? $formatted_address : $data['country'];
                    }
                }
            }
        }

        return $this->respond($data);
    }

    /**
     * Reverse geocoding using here.com
     *
     * @param array $data
     * @param string $latlng
     * @return string
     *
     */
    private function _reverse_here(array $data, string $latlng)
    {
        $cacheName = 'reverse-here_' . $latlng;
        if (! $result = cache($cacheName))
        {
            $client = \Config\Services::curlrequest(['base_uri' => 'https://revgeocode.search.hereapi.com/v1/']);
            $response = $client->request('get', 'revgeocode', [
                                    'query' => [
                                        'at' => $latlng,
                                        'lang' => 'id-ID',
                                        'apiKey' => env('HERE_API_KEY')
                                    ],
                                    'http_errors' => false
                                ]);

            $status = $response->getStatusCode();
            $reason = $response->getReason();

            if ($status === 200)
            {
                $body = $response->getBody();
                if (strpos($response->getHeader('content-type'), 'application/json') !== false)
                {
                    $result = json_decode($body, true);
                }

                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
            else
            {
                return $this->fail($reason, $status);
            }
        }

        $items = $result['items'];
        if (count($items) > 0)
        {
            foreach ($items[0]['address'] as $key => $value) {
                $data['street'] = ($key == 'label') ? $value : $data['street'];
                $data['subdistrict'] = ($key == 'subdistrict') ? $value : $data['subdistrict'];
                $data['district'] = ($key == 'district') ? $value : $data['district'];
                $data['city'] = ($key == 'city') ? $value : $data['city'];
                $data['county'] = ($key == 'county') ? $value : $data['county'];
                $data['country'] = ($key == 'countryName') ? $value : $data['country'];
            }
        }

        return $this->respond($data);
    }

    /**
     * Reverse geocoding using locationiq.com
     *
     * @param array $data
     * @param string $latlng
     * @return string
     *
     */
    private function _reverse_locationiq(array $data, string $latlng)
    {
        $_latlng = explode(',', $latlng, 2);
        $lat = $_latlng[0];
        $lng = $_latlng[1];

        $cacheName = 'reverse-locationiq_' . $latlng;
        if (! $result = cache($cacheName))
        {
            $client = \Config\Services::curlrequest(['base_uri' => 'https://us1.locationiq.com/v1/']);
            $response = $client->request('get', 'reverse.php', [
                                    'query' => [
                                        'lat' => $lat,
                                        'lon' => $lng,
                                        'format' => 'json',
                                        'accept_language' => 'id',
                                        'key' => env('LOCATIONIQ_API_KEY')
                                    ],
                                    'http_errors' => false
                                ]);

            $status = $response->getStatusCode();
            $reason = $response->getReason();

            if ($status === 200)
            {
                $body = $response->getBody();
                if (strpos($response->getHeader('content-type'), 'application/json') !== false)
                {
                    $result = json_decode($body, true);
                }

                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
            else
            {
                return $this->fail($reason, $status);
            }
        }

        $data['street'] = $result['display_name'];
        foreach ($result['address'] as $key => $value) {
            $data['subdistrict'] = ($key == 'suburb') ? $value : $data['subdistrict'];
            $data['district'] = ($key == 'city_district') ? $value : $data['district'];
            $data['city'] = ($key == 'city') ? $value : $data['city'];
            $data['county'] = ($key == 'state') ? $value : $data['county'];
            $data['country'] = ($key == 'country') ? $value : $data['country'];
        }

        return $this->respond($data);
    }
}