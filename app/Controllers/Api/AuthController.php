<?php namespace App\Controllers\Api;

/*
 * File: AuthController.php
 * Project: -
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 24th September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\Api\BaseApiController;
use LPI\Auth\Models\LoginModel;
use LPI\Auth\Models\UserModel;

class AuthController extends BaseApiController
{
	private function recordLoginAttempt(string $email, int $id = null, bool $success = false)
	{
		$request = \Config\Services::request();

		$ipAddress = $request->getIPAddress();
		$agent = $request->getUserAgent();
		if ($agent->isBrowser()) {
			$currentAgent = $agent->getBrowser() . ' ' . $agent->getVersion() . ' on ' . $agent->getPlatform();
		} elseif ($agent->isRobot()) {
			$currentAgent = $agent->getRobot();
		} elseif ($agent->isMobile()) {
			$currentAgent = $agent->getMobile();
		} else {
			$currentAgent = $agent->getAgentString();
		}

		$data = [
            'ip_address' => $ipAddress,
            'email' => $email,
            'user_id' => $id,
			'user_agent' => $currentAgent,
            'date' => date('Y-m-d H:i:s'),
            'success' => (int) $success
        ];

		$loginModel = new LoginModel();
		if (! $loginModel->save($data))
		{
			log_message("error", implode(" ", $loginModel->errors()));

			return false;
		}

		return true;
	}
	/**
	 * @apiDefine MyHeaderExample
	 * @apiHeaderExample {json} Header-Example:
     *      {
     *          "Authorization" : "Bearer <token>",
     *          "Content-Type" : "application/json"
     *      }
	 */

	/**
	 * @api {post} /login Create token user
	 * @apiDescription Validate to verify the user's credentials and retrieve authorization token.
     * @apiName PostLogin
     * @apiGroup Authentication
	 * @apiPermission none
     * @apiVersion 1.0.0
	 *
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
	 * @apiUse MyHeaderExample
	 *
     * @apiParam (Json) {String} login Mandatory Login with username or email address.
     * @apiParam (Json) {String} password Mandatory Password.
     *
     * @apiSuccess {String} message Message response.
     * @apiSuccess {Number} uid User ID.
     * @apiSuccess {String} access_token Access token value.
     * @apiSuccess {Number} expires Token expires.
	 *
     * @apiSampleRequest off
	 */
	public function attemptLogin()
	{
		$authConfig = new \Config\Auth();

		$rules = [
			'login'	=> 'required',
			'password' => 'required',
		];
		if ($authConfig->validFields == ['email'])
		{
			$rules['login'] .= '|valid_email';
		}

		if (! $this->validate($rules))
		{
			// Data did not validate
			return $this->failValidationErrors($this->validator->getErrors());
		}

		$postData = $this->request->getJSON(true);
		$login = $postData['login'] ?? '';
		$password = $postData['password'] ?? '';

		// Determine credential type
		$type = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

		// Try to log them in...
		$auth = service('authentication', 'local', new UserModel());
		if (! $user = $auth->validate([$type => $login, 'password' => $password], true))
		{
			// record a login attempt
			$this->recordLoginAttempt($login, $user->id ?? null,false);
			return $this->failValidationErrors($auth->error() ?? lang('Auth.badAttempt'));
		}

		if ($user->isBanned())
		{
			// record a login attempt
			$this->recordLoginAttempt($user->email, $user->id ?? null,false);
			return $this->fail(lang('Auth.userIsBanned'));
		}

		if (! $user->isActivated())
        {
			// record a login attempt
			$this->recordLoginAttempt($user->email, $user->id, false);
			$param = http_build_query([
                'login' => urlencode($login)
            ]);

			return $this->fail(lang('Auth.notActivated') .': '. anchor(route_to('resend-activate-account').'?'.$param, lang('Auth.activationResend')));
		}

		// Is the user being forced to reset their password?
		if ($user->force_pass_reset === true)
		{
			// record a login attempt
			$this->recordLoginAttempt($user->email, $user->id, false);
			return $this->fail('Please reset your password first: '. anchor(route_to('reset-password') .'?token='. $user->reset_hash));
		}

		// record a login attempt
		$this->recordLoginAttempt($user->email, $user->id, true);

		$config = service('lcobucci', ['algorithm' => 'asymmetric', 'method' => 'sign']);
		$now = new \DateTimeImmutable();
		$expires = $now->modify('+2 hour');
		$token = $config->builder()
						->issuedBy('https://linindopacific.com')
						->issuedAt($now)
						->expiresAt($expires)
						->relatedTo($user->id)
						->withClaim('uid', $user->uid)
						->withClaim('nik', $user->nik)
						->getToken($config->signer(), $config->signingKey());

		return $this->respond([
			'message' => lang('Auth.loginSuccess'),
			'uid' => (int) $user->id,
			'access_token' => $token->toString(),
			'expires' => $expires->getTimestamp()
		]);
	}

	/**
	 * @api {post} /forgot Forgot users password
	 * @apiDescription Attempts to find a user account with that password and retrieve link reset instructions.
     * @apiName PostForgotPassword
     * @apiGroup User
	 * @apiPermission none
     * @apiVersion 1.0.0
	 *
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
	 * @apiUse MyHeaderExample
	 *
     * @apiParam (Json) {Number} nik Mandatory users NIK.
     *
     * @apiSuccess {String} token Reset token value.
     * @apiSuccess {Number} expires Token expires.
     * @apiSuccess {String} uri Link reset instructions.
	 *
     * @apiSampleRequest off
	 */
	public function attemptForgot()
	{
		$authConfig = new \Config\Auth();

		if ($authConfig->activeResetter === null)
		{
			return $this->fail(lang('Auth.forgotDisabled'));
		}

		$rules = [
			'nik' => 'required|integer'
		];
		if (! $this->validate($rules))
		{
			// Data did not validate
			return $this->failValidationErrors($this->validator->getErrors());
		}

		$users = model(UserModel::class);

		$user = $users->where('username', $this->request->getJsonVar('nik'))->first();

		if (is_null($user))
		{
			return $this->fail(lang('Auth.forgotNoUsername'));
		}

		// Save the reset hash /
		$user->generateResetHash();
		$users->save($user);

		$resetter = service('resetter');
		$sent = $resetter->send($user);

		if (! $sent)
		{
			log_message('error', $resetter->error() ?? lang('Auth.unknownError'));
		}

		return $this->respond([
			'token' => $user->reset_hash,
			'expires' => $user->reset_expires->getTimestamp(),
			'uri' => site_url(route_to('reset-password') .'?token='. $user->reset_hash)
		]);
	}

	/**
	 * @api {post} /reset-password Reset users password
	 * @apiDescription Verifies the code with the NIK and saves the new password, if they all pass validation.
     * @apiName PostResetPassword
     * @apiGroup User
	 * @apiPermission none
     * @apiVersion 1.0.0
	 *
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
	 * @apiUse MyHeaderExample
	 *
     * @apiParam (Json) {String} token Mandatory token reset.
     * @apiParam (Json) {Number} nik Mandatory users NIK.
     * @apiParam (Json) {String} password Mandatory users new password.
     *
     * @apiSuccess {String} message
	 *
     * @apiSampleRequest off
	 */
	public function attemptReset()
	{
		$authConfig = new \Config\Auth();

		if ($authConfig->activeResetter === null)
		{
			return $this->fail(lang('Auth.forgotDisabled'));
		}

		$users = model(UserModel::class);

		$rules = [
			'token' => 'required',
			'nik' => 'required|integer',
			'password' => 'required|strong_password'
		];

		if (! $this->validate($rules))
		{
			return $this->failValidationErrors($this->validator->getErrors());
		}

		// First things first - log the reset attempt.
		$users->logResetAttempt(
			$this->request->getJsonVar('nik'),
			$this->request->getJsonVar('token'),
			$this->request->getIPAddress(),
			(string)$this->request->getUserAgent()
		);

		$user = $users->where('username', $this->request->getJsonVar('nik'))
					  ->where('reset_hash', $this->request->getJsonVar('token'))
					  ->first();

		if (is_null($user))
		{
			return $this->fail(lang('Auth.forgotNoUsername'));
		}

        // Reset token still valid?
        if (! empty($user->reset_expires) && time() > $user->reset_expires->getTimestamp())
        {
			return $this->fail(lang('Auth.resetTokenExpired'));
        }

		// Success! Save the new password, and cleanup the reset hash.
		$user->password = $this->request->getJsonVar('password');
		$user->reset_hash = null;
		$user->reset_at = date('Y-m-d H:i:s');
		$user->reset_expires = null;
        $user->force_pass_reset = false;
		$users->save($user);

		return $this->respond(['message' => lang('Auth.resetSuccess')]);
	}
}