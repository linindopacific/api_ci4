<?php namespace App\Controllers\Api;

/*
 * File: TimezoneController.php
 * Project: -
 * File Created: Wednesday, 4th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\Api\BaseApiController;
use CodeIgniter\I18n\Time;

class TimezoneController extends BaseApiController
{
    /**
     * @api {get} /timezone Read timezone
     * @apiDescription Read timeZone location based on position.
     * @apiName GetTimeZoneLocation
     * @apiGroup TimeZone
     * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
     *
     * @apiQuery {String="bing","google","timezonedb"} provider Mandatory Provider.
     * @apiQuery {String} latlng Mandatory Latitude Longitude.
     *
     * @apiSuccess {Number} gmtOffset The time offset in seconds based on UTC time.
     * @apiSuccess {String} zoneName The time zone name.
     * @apiSuccess {String} abbreviation Abbreviation of the time zone.
     * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     *      "gmtOffset": 28800,
     *      "zoneName": "Asia/Makassar",
     *      "abbreviation": "Waktu Indonesia Tengah"
     *  }
     *
     * @apiSampleRequest off
     */
	public function getTimeZone()
	{
		$provider = $this->request->getGet('provider');
		$latlng = $this->request->getGet('latlng');

        $validation = service('validation');
        $validation->setRules([
            'provider' => 'required|in_list[bing,google,timezonedb]',
            'latlng' => ['label' => 'latitude longitude', 'rules' => 'required'],
        ]);

        if (! $validation->run(['provider' => $provider, 'latlng' => $latlng]))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

		$data = array(
			'gmtOffset' => null,
			'zoneName' => null,
			'abbreviation' => null
		);

        $_latlng = explode(',', $latlng, 2);
        $_lat = (float) $_latlng[0];
        $_lng = (float) $_latlng[1];
        $latlng = strval($_lat) . ',' . strval($_lng);

        if ($provider === 'google')
        {
            return $this->_timeZone_google($data, $latlng);
        }
        elseif ($provider === 'bing')
        {
            return $this->_timeZone_bing($data, $latlng);
        }
        elseif ($provider === 'timezonedb')
        {
            return $this->_timeZone_timezonedb($data, $latlng);
        }
	}

    /**
     * Get timeZone using google.com
     *
     * @param array $data
     * @param string $latlng
     * @return string
     *
     */
	private function _timeZone_google(array $data, string $latlng)
	{
		$myTime = new Time('now');

		$cacheName = 'timeZone-google_' . $latlng;
        if (! $result = cache($cacheName))
        {
            $client = \Config\Services::curlrequest();
            $response = $client->request('get', 'https://maps.googleapis.com/maps/api/timezone/json', [
                'query' => [
                    'location' => $latlng,
                    'timestamp' => $myTime->getTimestamp(),
                    'language' => 'id',
                    'key' => env('GOOGLE_API_KEY')
                ],
                'http_errors' => false
            ]);

            $body = $response->getBody();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $result = json_decode($body, true);
            }

            if ($result['status'] === 'OK')
            {
                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
            else
            {
                return $this->fail($result['status']);
            }
        }

		foreach ($result as $key => $value) {
            $data['gmtOffset'] = ($key == 'rawOffset') ? $value : $data['gmtOffset'];
            $data['zoneName'] = ($key == 'timeZoneId') ? $value : $data['zoneName'];
            $data['abbreviation'] = ($key == 'timeZoneName') ? $value : $data['abbreviation'];
        }

        return $this->respond($data);
	}

    /**
     * Get timeZone using timezonedb.com
     *
     * @param array $data
     * @param string $latlng
     * @return string
     *
     */
	private function _timeZone_timezonedb(array $data, string $latlng)
	{
		$cacheName = 'timeZone-timezonedb_' . $latlng;
        if (! $result = cache($cacheName))
        {
            $client = \Config\Services::curlrequest();
            $response = $client->request('get', 'http://api.timezonedb.com/v2.1/get-time-zone', [
                'query' => [
					'by' => 'position',
                    'lat' => explode(',', $latlng, 2)[0],
                    'lng' => explode(',', $latlng, 2)[1],
					'format' => 'json',
                    'key' => env('TIMEZONEDB_API_KEY')
                ],
                'http_errors' => false
            ]);

            $body = $response->getBody();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $result = json_decode($body, true);
            }

            if ($result['status'] === 'OK')
            {
                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
            else
            {
                return $this->fail($result['message']);
            }
        }

		foreach ($result as $key => $value) {
            $data['gmtOffset'] = ($key == 'gmtOffset') ? $value : $data['gmtOffset'];
            $data['zoneName'] = ($key == 'zoneName') ? $value : $data['zoneName'];
            $data['abbreviation'] = ($key == 'abbreviation') ? $value : $data['abbreviation'];
        }

        return $this->respond($data);
	}

    /**
     * Get timeZone using Bing Maps
     *
     * @param array $data
     * @param string $latlng
     * @return string
     *
     */
	private function _timeZone_bing(array $data, string $latlng)
	{
		$cacheName = 'timeZone-bing_' . $latlng;
        if (! $result = cache($cacheName))
        {
            $client = \Config\Services::curlrequest();
            $response = $client->request('get', 'https://dev.virtualearth.net/REST/v1/TimeZone/'.$latlng, [
                'query' => [
					'output' => 'json',
                    'key' => env('BING_API_KEY')
                ],
                'http_errors' => false
            ]);

            $status = $response->getStatusCode();
            $reason = $response->getReason();

            if ($status === 200)
            {
                $body = $response->getBody();
                if (strpos($response->getHeader('content-type'), 'application/json') !== false)
                {
                    $result = json_decode($body, true);
                }

                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
            else
            {
                return $this->fail($reason);
            }
        }

        $results = $result['resourceSets'][0];

		foreach ($results['resources'][0]['timeZone'] as $key => $value) {
            if ($key == 'utcOffset')
            {
                $_value = explode(':', $value, 2);
                $_value_h = (int) $_value[0] * 3600;
                $_value_m = (int) $_value[1] * 60;
                $data['gmtOffset'] =  $_value_h + $_value_m;
            }
            $data['zoneName'] = ($key == 'ianaTimeZoneId') ? $value : $data['zoneName'];
            $data['abbreviation'] = ($key == 'abbreviation') ? $value : $data['abbreviation'];
        }

        return $this->respond($data);
	}
}
