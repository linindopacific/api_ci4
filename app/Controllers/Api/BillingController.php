<?php namespace App\Controllers\Api;

/*
 * File: BillingController.php
 * Project: -
 * File Created: Monday, 9th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 10th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\Billing;
use App\Controllers\Api\BaseApiController;
use App\Models\BillingModel;

class BillingController extends BaseApiController
{
	/**
     * @api {get} /billing/:id Get billings
     * @apiDescription Get billings info.
     * @apiName GetBilling
     * @apiGroup Billing
	 * @apiPermission none
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Optional billings unique ID.
     * @apiParam (Json) {String} services Optional service name.
     * @apiParam (Json) {String} vendor Optional service description.
     * @apiParam (Json) {String="DOMAIN","EMAIL_HOSTING","WEB_HOSTING"} type Optional type of service.
     * @apiParamExample {json} Request-Example:
     *      {
     *          "type" : "WEB_HOSTING"
     *      }
     *
     * @apiSampleRequest off
     */
	public function getBillings($id = NULL)
	{
		$model = new BillingModel();

        $data = $this->request->getJSON(true);
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                try
				{
                    is_null($model->findColumn($key));
                    $model->where($key, $value);
                }
				catch (\Exception $e)
				{
                    return $this->fail($e->getMessage());
                }
            }
        }

		$response  = $id ? $model->where('id', $id)->findAll() : $model->findAll();

		return $this->respond($response);
	}

	/**
     * @api {post} /billing Create new billing
     * @apiDescription Create a new billing info.
     * @apiName PostBilling
     * @apiGroup Billing
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
     * @apiParam (Json) {String} services Mandatory service name.
     * @apiParam (Json) {String} description Optional service description.
     * @apiParam (Json) {String} vendor Mandatory service vendor.
     * @apiParam (Json) {String="DOMAIN","EMAIL_HOSTING","WEB_HOSTING"} type Mandatory type of service.
     * @apiParam (Json) {String} expired_at Mandatory expired date of service.
	 *
     * @apiSampleRequest off
     */
	public function createBillings()
	{
		$model = new BillingModel();

		if (! $this->validate($model->getValidationRules()) )
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }

		try
		{
			$billing = new Billing($this->request->getJSON(true));
			if (! $model->save($billing))
			{
				return $this->fail($model->errors());
			}
		}
		catch (\Exception $e)
		{
			return $this->failServerError($e->getMessage());
		}

        return $this->respondCreated($billing);
	}

	/**
     * @api {put} /billing/:id Update billing
     * @apiDescription Update billing info.
     * @apiName PutBilling
     * @apiGroup Billing
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Mandatory billings unique ID.
     * @apiParam (Json) {String} services Service name.
     * @apiParam (Json) {String} description Service description.
     * @apiParam (Json) {String} vendor Service vendor.
     * @apiParam (Json) {String="DOMAIN","EMAIL_HOSTING","WEB_HOSTING"} type Type of service.
	 * @apiParam (Json) {String} expired_at Expired date of service.
	 *
     * @apiSampleRequest off
     */
	public function updateBillings($id)
	{
		$model = new BillingModel();

		$billing = $model->find($id);
		if (empty($billing))
		{
			return $this->failNotFound();
		}
		else
		{
			$data = $this->request->getJSON(true);
			if ($data)
			{
				foreach ($data as $key => $value)
				{
					try
					{
						is_null($model->findColumn($key));
						$billing->$key = $value;
					}
					catch (\Exception $e)
					{
						return $this->fail($e->getMessage());
					}
				}

				if ($billing->hasChanged())
				{
					if (! $this->validate([
						'services' => 'permit_empty|max_length[100]',
						'description' => 'permit_empty|max_length[255]',
						'vendor' => 'permit_empty|alpha_space|max_length[100]',
						'type' => 'permit_empty|in_list[DOMAIN,EMAIL_HOSTING,WEB_HOSTING]',
						'expired_at' => 'permit_empty|valid_date[Y-m-d]',
					]))
					{
						return $this->failValidationErrors($this->validator->getErrors());
					}

					try
					{
						if (! $model->save($billing))
						{
							return $this->fail($model->errors());
						}
					}
					catch (\Exception $e)
					{
						return $this->failServerError($e->getMessage());
					}

					return $this->respondUpdated($billing);
				}

				return $this->respond([]);
			}

			return $this->respondNoContent();
		}
	}

	/**
     * @api {delete} /billing/:id Delete billing
     * @apiDescription Delete billing info.
     * @apiName DeleteBilling
     * @apiGroup Billing
	 * @apiPermission admin,superadmin
     * @apiVersion 1.0.0
     *
     * @apiHeader {String} Authorization Authorization bearer.
     * @apiHeader {String="application/json"} Content-Type The content type of the resource.
     * @apiUse MyHeaderExample
	 *
	 * @apiParam {Number} id Mandatory billings unique ID.
	 *
     * @apiSampleRequest off
     */
	public function deleteBillings($id)
	{
		$model = new BillingModel();

        $billing = $model->find($id);
		if (empty($billing))
		{
			return $this->failNotFound();
		}
		else
		{
			if (! $model->delete($id))
			{
				return $this->fail($model->errors());
			}
		}

		return $this->respondDeleted([]);
	}
}