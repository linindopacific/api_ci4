<?php namespace App\Controllers\Api;

/*
 * File: CalendarController.php
 * Project: -
 * File Created: Friday, 6th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 21st October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\Api\BaseApiController;
use CodeIgniter\I18n\Time;

class CalendarController extends BaseApiController
{
	/**
     * @api {get} /calendar/:date Get calendar date
     * @apiDescription Get date info from calendar KalenderIndonesia.
     * @apiName GetDate
     * @apiGroup Calendar
	 * @apiPermission none
     * @apiVersion 1.0.0
	 *
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
	 * @apiUse MyHeaderExample
	 *
     * @apiParam {String} date=today Optional search of date with format: Y-m-d.
	 *
	 * @apiSuccess {Boolean} isHoliday National holiday status.
	 * @apiSuccess {String} holidayName National holiday name.
	 * @apiSuccess {String} textMasehi The text of Masehi calendar.
	 * @apiSuccess {String} textHijriyah The text of Hijriyah calendar.
	 * @apiSuccess {String} source Source credit calendar.
	 *
     * @apiSampleRequest off
     */
	public function getCalendar(string $str_tanggal = null)
	{
		$validation = service('validation');
		$validation->setRules([
            'date' => ['label' => 'Date', 'rules' => 'permit_empty|valid_date[Y-m-d]'],
        ]);
		if (! $validation->run(['date' => $str_tanggal]))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

		$myTime = $str_tanggal ? Time::createFromFormat('Y-m-d', $str_tanggal) : Time::now();

		$str_tanggal = $myTime->toDateString();
		$bulan = $myTime->getMonth();
		$tahun = $myTime->getYear();

		$cacheName = 'calendar_' . $tahun;
        if (! $result = cache($cacheName))
        {
			$baseUri = 'https://kalenderindonesia.com/api/' . env('KALENDER_API_KEY') . '/';
			$client = \Config\Services::curlrequest(['base_uri' => $baseUri]);
            $response = $client->request('get', 'kalender/masehi/' . $tahun, [
                'http_errors' => false,
				'verify' => false
            ]);

            $body = $response->getBody();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $result = json_decode($body, true);
            }

            if ($result['success'] === 1)
            {
                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
			else
			{
				return $this->fail($result['message']);
			}
		}

		$daily = $result['data']['monthly'][$bulan]['daily'];
		$daily_idx = null;
		for ($i=0; $i < count($daily); $i++)
		{
			foreach ($daily[$i]['date'] as $key => $value)
			{
				if ($key === 'M' && $value === $str_tanggal)
				{
					$daily_idx = $i;
					break;
				}
			}
		}

		$textMasehi = $daily[$daily_idx]['text']['M'];
		$textHijriyah = $daily[$daily_idx]['text']['H'];

		$isHoliday = false; $holidayName = '';
		$holiday = $daily[$daily_idx]['holiday'];
		$holiday_idx = null;
		if ($holiday)
		{
			for ($j=0; $j < count($holiday); $j++)
			{
				foreach ($holiday[$j] as $key => $value)
				{
					$isHoliday = ($key === 'date' && $value === $str_tanggal);
					if ($isHoliday)
					{
						$holiday_idx = $j;
						break;
					}
				}
			}

			$holidayName = $holiday[$holiday_idx]['name'];
		}

		$data = array(
			'isHoliday' => $isHoliday,
			'holidayName' => $holidayName,
			'textMasehi' => $textMasehi,
			'textHijriyah' => $textHijriyah,
			'source' => 'https://kalenderindonesia.com/kalender/masehi/' . $tahun
		);

		return $this->respond($data);
	}

	/**
     * @api {get} /calendar-working/days Get working days
     * @apiDescription Get working days from calendar KalenderIndonesia.
     * @apiName GetWorkingDays
     * @apiGroup Calendar
	 * @apiPermission none
     * @apiVersion 1.0.0
	 *
	 * @apiHeader {String="application/json"} Content-Type The content type of the resource.
	 * @apiUse MyHeaderExample
	 *
     * @apiQuery {String} start Mandatory start date with format: Y-m-d.
     * @apiQuery {String} end Mandatory end date with format: Y-m-d.
	 *
	 * @apiSuccess {Number} days Working days.
	 * @apiSuccess {Array} holidays The holidays.
	 * @apiSuccess {String} startDate
	 * @apiSuccess {String} endDate
	 * @apiSuccess {String} source Source credit calendar.
	 * @apiSuccessExample {json} Success-Response:
     *  HTTP/1.1 200 OK
     *  {
	 *     "days": 44,
	 *     "holidays": [
	 *         "2021-12-25",
	 *         "2022-01-01"
	 *     ],
	 *     "startDate": "01 Desember 2021",
	 *     "endDate": "31 Januari 2022",
	 *     "source": "https://kalenderindonesia.com"
	 *  }
	 *
     * @apiSampleRequest off
     */
	public function getWorkingDays()
    {
		$str_startDate = $this->request->getGet('start');
        $str_endDate = $this->request->getGet('end');

        $validation = service('validation');
        $validation->setRules([
            'start' => 'required|valid_date[Y-m-d]',
            'end' => 'required|valid_date[Y-m-d]'
        ]);

        if (! $validation->run(['start' => $str_startDate, 'end' => $str_endDate]))
        {
            return $this->failValidationErrors($validation->getErrors());
        }

		$time1 = Time::parse($str_startDate);
		$time2 = Time::parse($str_endDate);
		if ($time2->isBefore($time1))
		{
			return $this->fail('End date must be greater than start date.');
		}

		$getHolidays = null;
		$year1 = $time1->getYear();
		$year2 = $time2->getYear();
		if ($year1 === $year2)
		{
			$getHolidays = $this->getHolidays($year1);
		}
		else
		{
			$holiday1 = $this->getHolidays($year1);
			$holiday2 = $this->getHolidays($year2);
			$getHolidays = array_merge($holiday1, $holiday2);
		}

		if ($getHolidays === null)
		{
			return $this->failServerError();
		}

        // do strtotime calculations just once
        $endDate = $time2->getTimestamp();
        $startDate = $time1->getTimestamp();

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
          $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
		$holidays = [];
        foreach($getHolidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
				$workingDays--;

			//Show all holidays
			if ($startDate <= $time_stamp && $time_stamp <= $endDate)
				$holidays[] = $holiday;
        }

		$response = [
			'days' => $workingDays,
			'holidays' => $holidays,
			'startDate' => $time1->toLocalizedString('dd MMMM yyyy'),
			'endDate' => $time2->toLocalizedString('dd MMMM yyyy'),
			'source' => 'https://kalenderindonesia.com'
		];
        return $this->respond($response);
    }

	/**
	 * Get holiday function
	 *
	 * @param string $tahun
	 * @return array|null
	 */
	private function getHolidays(string $tahun)
    {
		$data = array();

        $cacheName = 'holiday_' . $tahun;
        if (! $result = cache($cacheName))
        {
			$baseUri = 'https://kalenderindonesia.com/api/' . env('KALENDER_API_KEY') . '/';
			$client = \Config\Services::curlrequest(['base_uri' => $baseUri]);
            $response = $client->request('get', 'libur/masehi/' . $tahun, [
                'http_errors' => false,
				'verify' => false
            ]);

            $body = $response->getBody();
            if (strpos($response->getHeader('content-type'), 'application/json') !== false)
            {
                $result = json_decode($body, true);
            }

            if ($result['success'] === 1)
            {
                // Save into the cache for 5 days
                cache()->save($cacheName, $result, 432000);
            }
			else
			{
				log_message('error', $result['message']);
				return null;
			}
		}

		$holidays = $result['data']['holiday'];
		foreach ($holidays as $holiday)
		{
			if ($holiday['count'] > 0)
			{
				for ($i=0; $i < count($holiday['data']); $i++)
				{
					foreach ($holiday['data'][$i] as $key => $value)
					{
						if ($key === 'date') $data[] = $value;
					}
				}
			}
		}

		return $data;
    }
}