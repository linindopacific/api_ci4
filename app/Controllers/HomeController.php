<?php namespace App\Controllers;

/*
 * File: HomeController.php
 * Project: -
 * File Created: Tuesday, 27th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 6th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;

class HomeController extends BaseController
{
	public function index()
	{
		helper('auth');

		return view('welcome', [
			'viewLayout' => 'App\Views\layout',
			'name' => user()->nik ?? ''
		]);
	}
}
