<?php

use LPI\Auth\Models\UserModel;

if (! function_exists('has_permission_jwt'))
{
	/**
	 * Ensures that the current user has the passed in permission.
	 * The permission can be passed in either as an ID or name.
	 *
	 * @param int|string $permission
	 *
	 * @return bool
	 */
	function has_permission_jwt($permission): bool
	{
		$authenticate = service('authentication', 'jwt');
        $authorize    = service('authorization', null, null, new UserModel());

        if ($authenticate->check())
        {
            return $authorize->hasPermission($permission, $authenticate->id()) ?? false;
        }

        return false;
	}
}