<!doctype html>
<html lang="en" class="h-100">
    <head>
        <?= view('App\Views\_header') ?>
    </head>
    <body class="d-flex flex-column h-100">
        <!-- Begin page content -->
        <main role="main" class="flex-shrink-0">
            <?= $this->renderSection('main') ?>
        </main>

        <?= view('App\Views\_footer') ?>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.min.js" integrity="sha384-PsUw7Xwds7x08Ew3exXhqzbhuEYmA2xnwc8BuD6SEr+UmEHlX8/MCltYEodzWA4u" crossorigin="anonymous"></script>
        <?= $this->renderSection('pageScripts') ?>
    </body>
</html>