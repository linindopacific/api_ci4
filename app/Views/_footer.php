<footer class="footer mt-auto py-3 bg-light">
    <div class="container">
        <span class="text-muted">&copy; <?= date('Y') ?>. Page rendered in {elapsed_time} seconds.</span>
        <span class="text-muted">Environment: <?= ENVIRONMENT ?>.</span>
    </div>
</footer>