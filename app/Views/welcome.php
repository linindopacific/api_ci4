<?= $this->extend($viewLayout) ?>
<?= $this->section('main') ?>

<div class="container">
    <h1 class="mt-5">Welcome</h1>
    <p class="lead">Hi, <?= $name ?>.</p>
</div>

<?= $this->endSection() ?>