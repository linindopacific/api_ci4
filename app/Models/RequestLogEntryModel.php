<?php namespace App\Models;

/*
 * File: RequestLogEntryModel.php
 * Project: -
 * File Created: Tuesday, 3rd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 3rd August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Model;

class RequestLogEntryModel extends Model
{
	protected $table = 'request_log_entries';

	protected $returnType = 'array';

	protected $allowedFields = [
		'method', 'path', 'body', 'user_id', 'ip_address', 'status', 'request_at'
	];

	protected $validationRules    = [
		'method' => 'required|alpha|max_length[10]',
        'path' => 'required|max_length[255]',
        'body' => 'permit_empty',
        'user_id' => 'permit_empty|integer|max_length[11]',
        'ip_address' => 'required|max_length[15]',
		'status' => 'required|integer|exact_length[3]',
		'request_at' => 'required',
    ];

    protected $validationMessages = [];

}