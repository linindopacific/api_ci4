<?php namespace App\Models;

/*
 * File: BillingModel.php
 * Project: Models
 * File Created: Monday, 9th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 9th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\Billing;
use CodeIgniter\Model;

class BillingModel extends Model
{
	protected $table = 'billings';

	protected $returnType = Billing::class;

	protected $allowedFields = [
		'services', 'description', 'vendor', 'type', 'expired_at'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'services' => 'required|max_length[100]',
        'description' => 'permit_empty|max_length[255]',
        'vendor' => 'required|alpha_space|max_length[100]',
		'type' => 'required|in_list[DOMAIN,EMAIL_HOSTING,WEB_HOSTING]',
		'expired_at' => 'required|valid_date[Y-m-d]',
	];

	protected $validationMessages = [];

}