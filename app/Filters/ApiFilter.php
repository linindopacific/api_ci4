<?php

namespace App\Filters;

/*
 * File: ApiFilter.php
 * Project: -
 * File Created: Monday, 2nd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 11th October 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Events\Events;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class ApiFilter implements FilterInterface
{
	/**
	 * Do whatever processing this filter needs to do.
	 * By default it should not return anything during
	 * normal execution. However, when an abnormal state
	 * is found, it should return an instance of
	 * CodeIgniter\HTTP\Response. If it does, script
	 * execution will end and that Response will be
	 * sent back to the client, allowing for error pages,
	 * redirects, etc.
	 *
	 * @param RequestInterface $request
	 * @param array|null       $arguments
	 *
	 * @return mixed
	 */
	public function before(RequestInterface $request, $arguments = null)
	{
		$response = \Config\Services::response();
		$response->setStatusCode(401);
		$data = ["status" => 401, "error" => 401];

		if ($request->hasHeader("Authorization"))
		{
			list($type, $token) = explode(" ", $request->getHeaderLine("Authorization"), 2);
			if (strcasecmp($type, "Bearer") == 0)
			{
				$authentication = service("authentication", "jwt");
				// validate token first
				if (! $authentication->validate(["token" => $token]))
				{
					Events::trigger("ApiQuery", $request, 401);
					$data["messages"]["error"] = $authentication->error();
					return $response->setJSON($data);
				}

				// get sub of this user (id), already validated first
				$config = service("lcobucci", ["algorithm" => "asymmetric", "method" => "verify"]);
				$tokenParsed = $config->parser()->parse($token);
				$userId = $tokenParsed->claims()->get("sub");

				// check if any arguments
				if ($arguments)
				{
					$check = $this->check($request, $arguments, $userId);
					if (! $check)
					{
						Events::trigger("ApiQuery", $request, 401, $userId);
						$data["messages"]["error"] = "You don't have permission.";
						return $response->setJSON($data);
					}
				}

				Events::trigger("ApiQuery", $request, 200, $userId);
			}
			else
			{
				Events::trigger("ApiQuery", $request, 401);
				$data["messages"]["error"] = "Error Processing Authorization Bearer";
				return $response->setJSON($data);
			}
		}
		else
		{
			Events::trigger("ApiQuery", $request, 401);
			$data["messages"]["error"] = "Unauthorized";
			return $response->setJSON($data);
		}
	}

	/**
	 * Allows After filters to inspect and modify the response
	 * object as needed. This method does not allow any way
	 * to stop execution of other after filters, short of
	 * throwing an Exception or Error.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param array|null        $arguments
	 *
	 * @return mixed
	 */
	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
	{
		//
	}

	private function check(RequestInterface $request, array $arguments, int $userId) : bool
	{
		// check if user access his own data
		$uri = $request->uri;
		if ($uri->getTotalSegments() === 3 && $uri->getSegment(2) === 'user')
		{
			$segment3 = $uri->getSegment(3); // user id
			if ($segment3 <> '' && $segment3 == $userId)
			{
				return true;
			}
		}

		for ($i=0; $i < count($arguments); $i++)
		{
			$argument = $arguments[$i];

			$_tmp = explode("_", $argument, 2);
			if ( count($_tmp) === 2 )
			{
				list($type, $value) = $_tmp;
				$authorize = service('authorization');
				if ($type == 'p')
				{
					return $authorize->hasPermission($value, $userId) ?? false;
				}
				elseif ($type == 'g')
				{
					return $authorize->inGroup($value, $userId);
				}
			}
		}

		return false;
	}
}