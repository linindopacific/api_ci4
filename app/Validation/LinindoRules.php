<?php namespace App\Validation;

/*
 * File: LinindoRules.php
 * Project: LPI
 * File Created: Wednesday, 25th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Models\CompanyModel;

class LinindoRules
{
    public function is_valid_entity(string $str, string &$error = null): bool
    {
        $companyModel = new CompanyModel();
        $company = $companyModel->where('prefix', $str)->first();

        if (empty($company))
        {
            $error = lang('Validation.is_valid_entity');
            return false;
        }

        return true;
    }
}