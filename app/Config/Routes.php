<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override(function(){
    return view('errors/html/custom_404');
});
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'HomeController::index', ['filter' => 'login']);
$routes->get('test', 'TestController::index');
$routes->get('test/lcobucci', 'TestController::lcobucci');

/*
 * Myth:Auth routes file.
 */
$routes->group('', function ($routes) {
    // Login/out
    $routes->get('login', 'AuthController::login', ['as' => 'login']);
    $routes->post('login', 'AuthController::attemptLogin');
    $routes->get('logout', 'AuthController::logout');

    // Registration
    $routes->get('register', 'AuthController::register', ['as' => 'register']);
    $routes->post('register', 'AuthController::attemptRegister');

    // Activation
    $routes->get('activate-account', 'AuthController::activateAccount', ['as' => 'activate-account']);
    $routes->get('resend-activate-account', 'AuthController::resendActivateAccount', ['as' => 'resend-activate-account']);

    // Forgot/Resets
    $routes->get('forgot', 'AuthController::forgotPassword', ['as' => 'forgot']);
    $routes->post('forgot', 'AuthController::attemptForgot');
    $routes->get('reset-password', 'AuthController::resetPassword', ['as' => 'reset-password']);
    $routes->post('reset-password', 'AuthController::attemptReset');
});

$routes->group('v1', function ($routes) {
    $routes->group('', ['namespace' => 'App\Controllers\Api'], function ($routes) {
        // Authentication
        $routes->post('login', 'AuthController::attemptLogin', ['filter' => 'api_free']);
        $routes->post('forgot', 'AuthController::attemptForgot', ['filter' => 'api_free']);
        $routes->post('reset-password', 'AuthController::attemptReset', ['filter' => 'api_free']);

        // Calendar
        $routes->get('calendar', 'CalendarController::getCalendar', ['filter' => 'api_free']);
        $routes->get('calendar/(:segment)', 'CalendarController::getCalendar/$1', ['filter' => 'api_free']);
        $routes->get('calendar-working/days', 'CalendarController::getWorkingDays', ['filter' => 'api_free']);

        // Location
        $routes->get('geocode-reverse', 'GeocodeController::getReverse', ['filter' => 'api_free']);
        $routes->get('timezone', 'TimezoneController::getTimeZone', ['filter' => 'api_free']);

        // Billing
        $routes->post('billing', 'BillingController::createBillings', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('billing', 'BillingController::getBillings', ['filter' => 'api']);
        $routes->get('billing/(:num)', 'BillingController::getBillings/$1', ['filter' => 'api']);
        $routes->put('billing/(:num)', 'BillingController::updateBillings/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->delete('billing/(:num)', 'BillingController::deleteBillings/$1', ['filter' => 'api:g_admin,g_superadmin']);
    });

    $routes->group('', ['namespace' => 'LPI\Controllers'], function ($routes) {
        // User
        $routes->post('user/in-groups', 'UserController::inGroups');
        $routes->post('user/has-permission', 'UserController::hasPermission');

        $routes->post('user', 'UserController::createUser', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('user', 'UserController::getUser', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('user/(:num)', 'UserController::getUser/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->put('user/(:num)', 'UserController::updateUser/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->delete('user/(:num)', 'UserController::deleteUser/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->post('user/ban/(:num)', 'UserController::banUser/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('user-nav', 'UserController::getUserNav', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('user-nav/(:segment)', 'UserController::getUserNav/$1', ['filter' => 'api:g_admin,g_superadmin']);

        // Company
        $routes->post('company', 'CompanyController::createCompany', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('company', 'CompanyController::getCompany', ['filter' => 'api']);
        $routes->get('company/(:num)', 'CompanyController::getCompany/$1', ['filter' => 'api']);
        $routes->put('company/(:num)', 'CompanyController::updateCompany/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->delete('company/(:num)', 'CompanyController::deleteCompany/$1', ['filter' => 'api:g_admin,g_superadmin']);
        $routes->get('company-nav', 'CompanyController::getCompanyNav', ['filter' => 'api']);
        $routes->get('company-nav/(:segment)', 'CompanyController::getCompanyNav/$1', ['filter' => 'api']);

        // Finance
        $routes->get('cash/payable', 'VendorLedgerEntryController::getPayable', ['filter' => 'api']);
        $routes->get('cash/receivable', 'CustomerLedgerEntryController::getReceivable', ['filter' => 'api']);

        // Logbook
        $routes->get('logbook', 'LogbookController::getCar', ['filter' => 'api_free']);
        $routes->get('logbook/(:num)', 'LogbookController::getCar/$1', ['filter' => 'api_free']);

        // Logistic
        $routes->post('inventory/stock', 'ItemLedgerEntryController::getSOH', ['filter' => 'api']);
    });
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
