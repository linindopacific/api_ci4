<?php

namespace Config;

use CodeIgniter\Config\BaseService;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Key\LocalFileReference;

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends BaseService
{
	// public static function example($getShared = true)
	// {
	//     if ($getShared)
	//     {
	//         return static::getSharedInstance('example');
	//     }
	//
	//     return new \CodeIgniter\Example();
	// }

	/**
	 * Lcobucci configuration
	 *
	 * @param array $config
	 * @param boolean $getShared
	 * @return Configuration
	 */
	public static function lcobucci(array $config = null, bool $getShared = true)
	{
		if ($getShared)
		{
			return static::getSharedInstance('lcobucci', $config);
		}

		if ($config)
		{
			if ($config['algorithm'] === 'asymmetric')
			{
				if ($config['method'] === 'sign')
				{
					$configuration =  Configuration::forAsymmetricSigner(
						new Signer\Rsa\Sha256(),
						LocalFileReference::file(ROOTPATH . 'rsa_private.pem'),
						InMemory::base64Encoded(env('JWT_KEY'))
					);
				}
				elseif ($config['method'] === 'verify')
				{
					$configuration =  Configuration::forAsymmetricSigner(
						new Signer\Rsa\Sha256(),
						LocalFileReference::file(FCPATH . 'rsa_public.pem'),
						InMemory::base64Encoded(env('JWT_KEY'))
					);
				}
			}
			elseif ($config['algoritm'] === 'symmetric')
			{
				$configuration = Configuration::forSymmetricSigner(
					new Signer\Rsa\Sha256(),
					InMemory::base64Encoded(env('JWT_KEY'))
				);
			}
		}
		else
		{
			$configuration = Configuration::forUnsecuredSigner();
		}

		return $configuration;
	}
}
