<?php namespace Config;

/*
 * File: Auth.php
 * Project: -
 * File Created: Tuesday, 27th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 23rd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use LPI\Auth\Config\Auth as AuthConfig;

class Auth extends AuthConfig
{
	/**
	 * --------------------------------------------------------------------
	 * Views used by Auth Controllers
	 * --------------------------------------------------------------------
	 *
	 * @var array
	 */
	public $views = [
		'login' => 'App\Views\Auth\login',
		'register' => 'App\Views\Auth\register',
		'forgot' => 'App\Views\Auth\forgot',
		'reset' => 'App\Views\Auth\reset',
		'emailForgot' => 'App\Views\Auth\emails\forgot',
		'emailActivation' => 'App\Views\Auth\emails\activation',
	];

	/**
	 * --------------------------------------------------------------------
	 * Layout for the views to extend
	 * --------------------------------------------------------------------
	 *
	 * @var string
	 */
	public $viewLayout = 'App\Views\Auth\layout';

	/**
	 * --------------------------------------------------------------------
	 * Password Check Helpers
	 * --------------------------------------------------------------------
	 *
	 * The PasswordValidater class runs the password through all of these
	 * classes, each getting the opportunity to pass/fail the password.
	 *
	 * You can add custom classes as long as they adhere to the
	 * Password\ValidatorInterface.
	 *
	 * @var string[]
	 */
	public $passwordValidators = [
		'Myth\Auth\Authentication\Passwords\CompositionValidator',
		// 'Myth\Auth\Authentication\Passwords\NothingPersonalValidator',
		'Myth\Auth\Authentication\Passwords\DictionaryValidator',
		// 'Myth\Auth\Authentication\Passwords\PwnedValidator',
	];

}