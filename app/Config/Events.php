<?php

namespace Config;

use App\Models\RequestLogEntryModel;
use CodeIgniter\Events\Events;
use CodeIgniter\Exceptions\FrameworkException;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\I18n\Time;

/*
 * --------------------------------------------------------------------
 * Application Events
 * --------------------------------------------------------------------
 * Events allow you to tap into the execution of the program without
 * modifying or extending core files. This file provides a central
 * location to define your events, though they can always be added
 * at run-time, also, if needed.
 *
 * You create code that can execute by subscribing to events with
 * the 'on()' method. This accepts any form of callable, including
 * Closures, that will be executed when the event is triggered.
 *
 * Example:
 *      Events::on('create', [$myInstance, 'myMethod']);
 */

Events::on('pre_system', function () {
	if (ENVIRONMENT !== 'testing')
	{
		if (ini_get('zlib.output_compression'))
		{
			throw FrameworkException::forEnabledZlibOutputCompression();
		}

		while (ob_get_level() > 0)
		{
			ob_end_flush();
		}

		ob_start(function ($buffer) {
			return $buffer;
		});
	}

	/*
	 * --------------------------------------------------------------------
	 * Debug Toolbar Listeners.
	 * --------------------------------------------------------------------
	 * If you delete, they will no longer be collected.
	 */
	if (CI_DEBUG && ! is_cli())
	{
		Events::on('DBQuery', 'CodeIgniter\Debug\Toolbar\Collectors\Database::collect');
		Services::toolbar()->respond();
	}

	Events::on('ApiQuery', function(RequestInterface $request, int $status, int $user_id = NULL)
	{
		$contentType = $request->getHeaderLine('content-type');
		$body = null;
		if ($contentType === 'application/json')
		{
			$_body = $request->getJSON(true);
			if (in_array($request->uri->getPath(), ['v1/login', 'v1/user']))
			{
				unset($_body['password']);
			}

			$body = json_encode($_body);
		}
		else
		{
			$body = json_encode($request->getPost());
		}

		$myTime = new Time('now');

		$path = $request->uri->getPath();
		$query = $request->uri->getQuery();
		$fullPath = ($query) ? $path . '?' . $query : $path;

		$data = [
			'method' => $request->getMethod(),
			'path' => $path,
			'body' => $body,
			'user_id' => $user_id,
			'ip_address' => $request->getIPAddress(),
			'status' => $status,
			'request_at' => $myTime->toDateTimeString()
		];
		$model = new RequestLogEntryModel();
		if(! $model->save($data) ) {
			log_message('error', implode(' ', $model->errors));
		}
	});
});
