<?php

namespace Config;

use App\Filters\ApiFilter;
use App\Filters\ApiFreeFilter;
use App\Filters\OptionsFilter;
use App\Filters\ThrottleFilter;
use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\CSRF;
use CodeIgniter\Filters\DebugToolbar;
use CodeIgniter\Filters\Honeypot;

class Filters extends BaseConfig
{
	/**
	 * Configures aliases for Filter classes to
	 * make reading things nicer and simpler.
	 *
	 * @var array
	 */
	public $aliases = [
		'csrf' => CSRF::class,
		'toolbar' => DebugToolbar::class,
		'honeypot' => Honeypot::class,
		'api' => ApiFilter::class,
		'api_free' => ApiFreeFilter::class,
		'options' => OptionsFilter::class,
		'throttle' => ThrottleFilter::class,
		'login' => \LPI\Auth\Filters\LoginFilter::class,
		'role' => \LPI\Auth\Filters\RoleFilter::class,
		'permission' => \LPI\Auth\Filters\PermissionFilter::class,
	];

	/**
	 * List of filter aliases that are always
	 * applied before and after every request.
	 *
	 * @var array
	 */
	public $globals = [
		'before' => [
			'options', //register option CORS
			'throttle',
			// 'honeypot',
			// 'csrf',
		],
		'after'  => [
			'toolbar',
			// 'honeypot',
		],
	];

	/**
	 * List of filter aliases that works on a
	 * particular HTTP method (GET, POST, etc.).
	 *
	 * Example:
	 * 'post' => ['csrf', 'throttle']
	 *
	 * @var array
	 */
	public $methods = [];

	/**
	 * List of filter aliases that should run on any
	 * before or after URI patterns.
	 *
	 * Example:
	 * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
	 *
	 * @var array
	 */
	public $filters = [];
}
