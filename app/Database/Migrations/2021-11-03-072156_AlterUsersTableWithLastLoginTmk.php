<?php

namespace App\Database\Migrations;

/*
 * File: 2021-11-03-072156_AlterUsersTableWithLastLoginTmk.php
 * Project: -
 * File Created: Wednesday, 3rd November 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class AlterUsersTableWithLastLoginTmk extends Migration
{
    protected $table = "users";

    public function up()
    {
        $fields = [
			'tmk_at' => ['type' => 'date', 'null' => true, 'after' => 'force_pass_reset'],
			'last_active_at' => ['type' => 'datetime', 'null' => true, 'after' => 'tmk_at'],
		];
		$this->forge->addColumn($this->table, $fields);
    }

    public function down()
	{
		// backup first
		$this->_backup();

		$this->forge->dropColumn($this->table, "last_active_at");
		$this->forge->dropColumn($this->table, "tmk_at");
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper("filesystem");
		$filename = $this->table . "_" . time() . ".csv";
		if (! write_file(WRITEPATH . "dbdump/" . $filename, $data))
		{
			log_message("error", "Unable to write the backup file");
			die("Unable to write the backup file");
		}
	}
}