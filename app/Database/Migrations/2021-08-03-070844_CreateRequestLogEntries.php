<?php namespace App\Database\Migrations;

/*
 * File: 2021-08-03-070844_CreateRequestLogEntries.php
 * Project: -
 * File Created: Tuesday, 3rd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 22nd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class CreateRequestLogEntries extends Migration
{
	protected $table = 'request_log_entries';

	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'unsigned' => true,
				'auto_increment' => true
			],
			'method' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => false
			],
			'path' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => false
			],
			'body' => [
				'type' => 'TEXT',
				'null' => true
			],
			'status' => [
				'type' => 'INT',
				'constraint' => 3,
				'null' => false
			],
			'ip_address' => [
				'type' => 'VARCHAR',
				'constraint' => 15,
				'null' => false
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => true
			],
			'request_at' => [
				'type' => 'datetime',
				'null' => true
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable($this->table, true);
	}

	public function down()
	{
		// backup first
		$this->_backup();

		$this->forge->dropTable($this->table, true);
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper('filesystem');
		$filename = $this->table . '_' . time() . '.csv';
		if (! write_file(WRITEPATH . 'dbdump/' . $filename, $data))
		{
			log_message('error', 'Unable to write the backup file');
			die('Unable to write the backup file');
		}
	}
}
