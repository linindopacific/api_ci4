<?php namespace App\Database\Migrations;

/*
 * File: 2021-07-27-042629_MigrationAlterTableUsers.php
 * Project: -
 * File Created: Tuesday, 27th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class MigrationAlterTableUsers extends Migration
{
	protected $table = 'users';

	public function up()
	{
		// add new identity info
		$fields = [
			'firstname' => ['type' => 'VARCHAR', 'constraint' => 63, 'after' => 'username'],
			'lastname' => ['type' => 'VARCHAR', 'constraint' => 63, 'after' => 'firstname'],
			'phone' => ['type' => 'VARCHAR', 'constraint' => 63, 'after' => 'lastname'],
			'uid' => ['type' => 'INT', 'constraint' => 10, 'unsigned' => true, 'null' => false, 'unique' => true, 'after' => 'id'],
		];
		$this->forge->addColumn($this->table, $fields);
	}

	public function down()
	{
		// backup first
		$this->_backup();

		// drop new columns
		$this->forge->dropColumn($this->table, 'firstname');
		$this->forge->dropColumn($this->table, 'lastname');
		$this->forge->dropColumn($this->table, 'phone');
		$this->forge->dropColumn($this->table, 'uid');
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper('filesystem');
		$filename = $this->table . '_' . time() . '.csv';
		if (! write_file(WRITEPATH . 'dbdump/' . $filename, $data))
		{
			log_message('error', 'Unable to write the backup file');
			die('Unable to write the backup file');
		}
	}
}