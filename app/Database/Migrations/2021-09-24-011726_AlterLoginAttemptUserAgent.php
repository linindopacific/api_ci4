<?php

namespace App\Database\Migrations;

/*
 * File: 2021-09-24-011726_AlterLoginAttemptUserAgent.php
 * Project: -
 * File Created: Friday, 24th September 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 24th September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class AlterLoginAttemptUserAgent extends Migration
{
    protected $table = "auth_logins";

    public function up()
    {
        $fields = [
            "user_agent" => [
                "type" => "varchar",
                "constraint" => 100,
                "null" => true,
                "after" => "user_id"
            ]
        ];
        $this->forge->addColumn($this->table, $fields);
    }

    public function down()
	{
		// backup first
		$this->_backup();

		$this->forge->dropColumn($this->table, "user_agent");
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper("filesystem");
		$filename = $this->table . "_" . time() . ".csv";
		if (! write_file(WRITEPATH . "dbdump/" . $filename, $data))
		{
			log_message("error", "Unable to write the backup file");
			die("Unable to write the backup file");
		}
	}
}