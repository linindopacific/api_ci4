<?php namespace App\Database\Migrations;

/*
 * File: 2021-08-09-030736_CreateBillingTable.php
 * Project: -
 * File Created: Monday, 9th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 22nd September 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Database;
use CodeIgniter\Database\Migration;

class CreateBillingTable extends Migration
{
	protected $table = 'billings';

	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'unsigned' => true,
				'auto_increment' => true
			],
			'services' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => false
			],
			'description' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => true
			],
			'vendor' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => false
			],
			'type' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => false
			],
			'expired_at' => [
				'type' => 'datetime',
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
			'updated_at' => [
				'type' => 'datetime',
				'null' => true
			],
			'deleted_at' => [
				'type' => 'datetime',
				'null' => true
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->addUniqueKey(['services', 'type']);
		$this->forge->createTable($this->table, true);
	}

	public function down()
	{
		// backup first
		$this->_backup();

		$this->forge->dropTable($this->table, true);
	}

	function _backup()
	{
		$db = db_connect($this->getDBGroup());
		$builder = $db->table($this->table);

		$util = (new Database())->loadUtils($db);
		$data = $util->getCSVFromResult($builder->get());

		helper('filesystem');
		$filename = $this->table . '_' . time() . '.csv';
		if (! write_file(WRITEPATH . 'dbdump/' . $filename, $data))
		{
			log_message('error', 'Unable to write the backup file');
			die('Unable to write the backup file');
		}
	}
}
