<?php namespace App\Database\Seeds;

/*
 * File: CompanySeeder.php
 * Project: -
 * File Created: Tuesday, 3rd August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class CompanySeeder extends Seeder
{
	protected $table = 'companies';

	public function run()
	{
		$builder = $this->db->table($this->table);

		$data = [
			[
				'name' => 'Dwikarya Linindo Teknik',
				'prefix' => 'DLT',
				'short' => 'D',
				'table' => 'Dwikarya Linindo Teknik$',
				'group' => 1,
				'priority' => 2,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Paz Ace Indonesia',
				'prefix' => 'PAI',
				'short' => 'A',
				'table' => 'Paz Ace Indonesia$',
				'group' => 1,
				'priority' => 4,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Persada MultiParts',
				'prefix' => 'PMP',
				'short' => 'M',
				'table' => 'Persada MultiParts$',
				'group' => 1,
				'priority' => 3,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Linindo Teknologi Int.',
				'prefix' => 'LTI',
				'short' => 'I',
				'table' => 'Linindo Teknologi Int_$',
				'group' => 2,
				'priority' => 5,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Multipedia Teknika Indonesia',
				'prefix' => 'MTI',
				'short' => 'E',
				'table' => 'Multipedia Teknika Indonesia $',
				'group' => 3,
				'priority' => 6,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Jomon Persada Nusantara',
				'prefix' => 'JMN',
				'short' => 'J',
				'table' => 'Jomon Persada Nusantara$',
				'group' => 4,
				'priority' => 7,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Linindo Pacific International',
				'prefix' => 'LPI',
				'short' => 'L',
				'table' => 'Linindo Pacific International$',
				'group' => 0,
				'priority' => 1,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Aruna International',
				'prefix' => 'ARN',
				'short' => 'R',
				'table' => 'Aruna International$',
				'group' => 5,
				'priority' => 8,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Allight Sykes Indonesia',
				'prefix' => 'ASI',
				'short' => 'S',
				'table' => 'Allightsykes Indonesia$',
				'group' => 6,
				'priority' => 9,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Total Artha Solusi Indonesia',
				'prefix' => 'TASI',
				'short' => 'T',
				'table' => 'Total Artha Solusi Indonesia$',
				'group' => 2,
				'priority' => 10,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Geo Spatial Nusantara',
				'prefix' => 'GSN',
				'short' => 'G',
				'table' => 'Geo Spatial Nusantara$',
				'group' => 7,
				'priority' => 11,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Amarta Bayu Caraka',
				'prefix' => 'ABC',
				'short' => 'B',
				'table' => 'Amarta Bayu Caraka$',
				'group' => 8,
				'priority' => 12,
				'created_at' => date('Y-m-d H:i:s'),
			],
			[
				'name' => 'Amarta Bayu Caraka, PT',
				'prefix' => 'ABCP',
				'short' => 'C',
				'table' => 'PT_ Amarta Bayu Caraka$',
				'group' => 9,
				'priority' => 13,
				'created_at' => date('Y-m-d H:i:s'),
			],
		];

		$builder->insertBatch($data);
	}
}