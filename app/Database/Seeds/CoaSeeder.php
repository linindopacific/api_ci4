<?php namespace App\Database\Seeds;

/*
 * File: CoaSeeder.php
 * Project: Seeds
 * File Created: Monday, 16th August 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class CoaSeeder extends Seeder
{
	protected $DBGroup = 'finance';

	protected $table = 'coas';

	public function run()
	{
		$path = WRITEPATH . 'uploads/finance_coa.csv';
		$file = new \CodeIgniter\Files\File($path);

		if ($file->isReadable())
		{
			ini_set('max_execution_time', 0);

			$csv = $file->openFile();
			$row = 0;
			$dt_column = array();
			$dt_row = array();
			$isActive = true;
			$coaModel = new \LPI\Models\CoaModel();
			while (! $csv->eof())
			{
				$line = $csv->fgets();
				$line_arr = explode(';', $line);
				if ($row == 0)
				{
					for ($i=0; $i < count($line_arr); $i++)
					{
						$idx = trim($line_arr[$i]);

						$dt_column[$idx] = null;
					}
				}
				else
				{
					$i = 0;
					foreach ($dt_column as $key => $value)
					{
						$newValue = trim($line_arr[$i]);
						$dt_row[$key] = trim($newValue);
						$i++;
					}
				}

				if ($row <> 0)
				{
					$isActive = ($dt_row['active'] == 0) ? false : true;
					unset($dt_row['active']);

					$coa = new \LPI\Entities\Coa($dt_row);

					if (! $id = $coaModel->insert($coa, true))
					{
						log_message('error', implode(' ', $coaModel->errors()));
						die(implode(' ', $coaModel->errors()));
					}

					if (! $isActive)
					{
						$coaModel->delete($id);
					}
				}

				$row++;
			}
		}
	}
}
