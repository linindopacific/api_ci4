<?php namespace App\Database\Seeds;

/*
 * File: RoleSeeder.php
 * Project: -
 * File Created: Tuesday, 27th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 16th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class RoleSeeder extends Seeder
{
	protected $table = 'auth_groups';

	public function run()
	{
		$builder = $this->db->table($this->table);

		$data = [
			[
				'name' => 'superadmin',
				'description' => 'Superadmin role'
			],
			[
				'name' => 'admin',
				'description' => 'Admin role'
			],
			[
				'name' => 'guests',
				'description' => 'Guests role'
			]
		];

		$builder->insertBatch($data);
	}
}