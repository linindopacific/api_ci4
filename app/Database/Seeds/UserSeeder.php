<?php namespace App\Database\Seeds;

/*
 * File: UserSeeder.php
 * Project: -
 * File Created: Friday, 30th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 3rd November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;
use LPI\Auth\Entities\User;
use LPI\Auth\Models\UserModel;

class UserSeeder extends Seeder
{
	public function run()
	{
		$path = WRITEPATH . 'uploads/default_users.csv';
		$file = new \CodeIgniter\Files\File($path);

		if ($file->isReadable())
		{
			ini_set('max_execution_time', 0);

			$csv = $file->openFile();
			$row = 0;
			$dt_column = array();
			$dt_row = array();
			$userModel = new UserModel();
			while (! $csv->eof())
			{
				$line = $csv->fgets();
				$line_arr = explode(';', $line);
				if ($row == 0)
				{
					for ($i=0; $i < count($line_arr); $i++)
					{
						$idx = trim($line_arr[$i], "\"");
						$dt_column[$idx] = null;
					}
				}
				else
				{
					$i = 0;

					foreach ($dt_column as $key => $value)
					{
						$newValue = trim($line_arr[$i], "\"");
						if ($key === "username" && $newValue === "")
						{
							$newValue = explode("@", $dt_row["email"])[0];
						}

						$dt_row[$key] = trim($newValue);
						$i++;
					}
				}

				// remove created_at field
				array_pop($dt_row);

				if ($row <> 0)
				{
					$customRules = [
						"uid" => "required|numeric",
						"email" => "required|valid_email|is_unique[users.email]",
						"username" => [
							"label" => "NIK",
							"rules" => "permit_empty|min_length[1]|max_length[30]|is_unique[users.username]"
						],
						"first_name" => "permit_empty|alpha_space|max_length[63]",
						"last_name" => "permit_empty|alpha_space|max_length[63]",
						"password_hash" => "required",
						"active" => "required|numeric",
						"phone" => "permit_empty|numeric",
						"tmk_at" => "permit_empty|valid_date[Y-m-d]",
						"last_active_at" => "permit_empty"
					];

					$validation =  \Config\Services::validation();
					$validation->setRules($customRules);
					if (! $validation->run($dt_row))
					{
						log_message("error", implode(" ", $validation->getErrors()));
						die(implode(" ", $validation->getErrors()));
					}

					$user = new User($dt_row);
					if ($dt_row["active"] == 0) {
						$user->ban("resign");
					}
					// Ensure default group gets assigned if set
					$config = new \Config\Auth();
					if (! empty($config->defaultUserGroup)) {
						if ($dt_row["username"] == 302) {
							$userModel = $userModel->withGroup("superadmin");
						} else {
							$userModel = $userModel->withGroup($config->defaultUserGroup);
						}
					}
					$userModel->skipValidation(); // we already check the rules above
					if (! $userModel->save($user))
					{
						log_message("error", implode(" ", $userModel->errors()));
						die(implode(" ", $userModel->errors()));
					}
				}

				$row++;
			}
		}
	}
}