<?php

// override core id language system validation or define your own id language validation message
return [
    // Forgotten Passwords
    'forgotNoUsername' => 'Tidak dapat menemukan pengguna dengan NIK tersebut.',

    // Login views
    'username' => 'NIK',
    'enterCodeUsernamePassword' => 'Masukkan kode yang Anda terima, NIK Anda, dan kata sandi baru Anda.',
];
